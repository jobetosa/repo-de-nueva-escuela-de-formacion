<?php $title = 'Certificados - Escuela de Formación' ?>
<?php ob_start() ?>
 <style>
	#form-ancho{
	max-width: 450px;
	display: inline-block;	
	}
	#form-ancho2{
	width: 62%;
	display: inline-block;
	margin-left: 20%;	
	}
	.grilla {
    background-color: #fff; 
    margin: 5px 0 10px 0; 
    border: solid 1px black; 
    border-collapse:collapse; 
}
.grilla td { 
    padding: 2px; 
    border: solid 1px black; 
	color: #717171; 
    text-align:center;
}
.grilla th { 
    padding: 4px 2px;
	background: gray repeat-x;
    border-left: solid 1px #525252; 
    font-size: 1 em; 
}

tr:nth-child(odd)		{ background-color:#eee; }
tr:nth-child(even)		{ background-color:#fff; }
.btn.btn-primary {width: 100%;background-color: #3c9900;border-color: #fff;}
.ocul {text-align: center; }
</style>	
<?php $estilo = ob_get_clean() ?>
<?php ob_start() ?>
	<div class="btn btn-primary"><a class="btn btn-primary" href="./generar">GENERAR CERTIFICADOS</a></div>
	<div class="table-responsive">
		<table class="table table-striped" id="tablajson">
			<thead>
				<th>ID</th>
				<th>NOMBRE</th>
				<th>APELLIDO</th>
				<th>CURSO CERTIFICADO</th>
				<th>DESCARGAR</th>                   
			</thead>
			<tbody>			
				<?php foreach ($estudiantes as $estudiante): ?>
					<tr>
						<td><?php echo $estudiante["id"] ?></td>
						<td><?php echo utf8_encode($estudiante["nombre"]) ?></td>
						<td><?php echo utf8_encode($estudiante["apellidos"]) ?></td>
						<td>Bases Conceptuales - Cambio Climatico</td>
						<td><div class="btn btn-primary" ><a class="btn btn-primary" href=".<?php echo $estudiante["path"]; ?>" download>Descargar</a></div></td>
					</tr>
				<?php endforeach ?>				
			</tbody>
		</table>
	</div>
<?php $contenido = ob_get_clean() ?>

<?php include 'base.php' ?>

<?php

set_time_limit(9600);

// index.php

// carga e inicia algunas librerias globales
require_once 'modelo.php';
require_once 'controlador.php';


//error_reporting(E_ALL ^ E_NOTICE);
// encamina la petición internamente
$uri = $_SERVER['REQUEST_URI'];
$urls = explode('?', $uri);
$uri = $urls[0];
$base_url  = '/certificados/index.php';
if ($uri == $base_url || $uri == $base_url.'/') {
    header('Location: ./buscarcertificado');
}
if ($uri == '/certificados' || $uri == '/certificados/' ) {
    header('Location: ./index.php/buscarcertificado');
}
elseif ($uri == $base_url.'/generar') {
    generarCertificadosAction();
}
//elseif ($uri == $base_url.'/admin') {
  //  adminAction();
//}
elseif ($uri == $base_url.'/buscarcertificado') {
    buscarCertificadoAction();
}
elseif ($uri == $base_url.'/buscar') {
    buscarAction();
}
elseif ($uri == $base_url.'/verificarcertificado') {
    verificarCertificadoAction();
}
elseif ($uri == $base_url.'/verificar') {
    verificarAction();
}
else {
    header('Status: 404 Not Found');
    echo '<html><body><h1>Page Not Found</h1></body></html>';
}

?>
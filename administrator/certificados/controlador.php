<?php
// controlador.php

function adminAction()
{
	$estudiantes = getEstudiantesCertificado();
	require 'templates/home.php';
}

function generarCertificadosAction()
{
	generarCertificados();
    header('Location: ./admin');
}

function buscarCertificadoAction(){	
	require 'templates/consulta.php';
}

function buscarAction(){
	$estudiante = buscarCertificado();
	header('Content-Type: application/json');
	echo json_encode($estudiante);
}

function verificarCertificadoAction(){
	require 'templates/verificar.php';
}

function verificarAction(){
	$certificado = verificarCertificado();
	header('Content-Type: application/json');
	echo json_encode($certificado);
}

?>

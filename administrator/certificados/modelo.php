<?php
// modelo.php
require_once 'tcpdf/tcpdf.php';
require_once 'tcpdf/config/tcpdf_config.php';

function abrirConexionBasededatos()
{
    $conexion = mysqli_connect('172.17.0.48', 'uproduccion', 'Cqbf8ERjmRuaXqsF');
    mysqli_select_db($conexion, 'certificados_escuela');
    if (mysqli_connect_errno())
    {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    return $conexion;
}

function cerrarConexionBasededatos($conexion)
{
    mysqli_close($conexion);
}

function getEstudiantes(){
    $conexion = abrirConexionBasededatos();
    $dataEstudiantes = mysqli_query($conexion, 'SELECT * FROM  estudiantes');
    //$dataEstudiantes = mysqli_query($conexion, 'SELECT * FROM  estudiantes LIMIT 300 OFFSET 300');
    $estudiantes = array();
    while ($row = mysqli_fetch_assoc($dataEstudiantes)) {
        $estudiantes[] = $row;
    }
    cerrarConexionBasededatos($conexion);

    return $estudiantes;
}

function getEstudiantesCertificado(){
    $conexion = abrirConexionBasededatos();
    $dataEstudiantes = mysqli_query($conexion, 'SELECT e.*, c.* FROM  estudiantes AS e INNER JOIN certificados AS c ON e.id = c.id_estudiante');
    $estudiantes = array();
    if($dataEstudiantes == false){
        return $estudiantes;
    }
    while ($row = mysqli_fetch_assoc($dataEstudiantes)) {
        $estudiantes[] = $row;
    }
    cerrarConexionBasededatos($conexion);

    return $estudiantes;
}

function generarCertificados(){
    $estudiantes = getEstudiantes();
    foreach ($estudiantes as $estudiante) {
        $codigo = md5(uniqid(time()));
        $path =  generarCertificado($estudiante, $codigo);
        setCertificado($estudiante['id'], $codigo, $path);
    }
}

function setCertificado($estudiante_id, $codigo, $path){
    $conexion = abrirConexionBasededatos();

    $nuevoGrupo="INSERT INTO certificados VALUES ('','$path','".time()."','$estudiante_id','$codigo')";

    if (!mysqli_query($conexion, $nuevoGrupo))
    {
        echo("Error description: " . mysqli_error($conexion)."<br>");
    }
    cerrarConexionBasededatos($conexion);
}

function generarCertificado($estudiante, $codigo){
    $pdf = new TCPDF('L', 'mm', 'A4', true, 'UTF-8', false);

    $fontname = TCPDF_FONTS::addTTFfont(__DIR__.'/fonts/Lato-Regular.ttf', 'TrueTypeUnicode', '', 32);
    $pdf->SetTitle("Certificado Bases Conceptuales");
    $pdf->SetProtection(array('modify'));
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->SetAutoPageBreak(false, 0);

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(0);
    $pdf->SetFooterMargin(0);

    // remove default footer
    $pdf->setPrintFooter(false);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


    $pdf->AddPage();    


    $bMargin = $pdf->getBreakMargin();
    // get current auto-page-break mode
    $auto_page_break = $pdf->getAutoPageBreak();
    // disable auto-page-break
    $pdf->SetAutoPageBreak(false, 0);
    // set bacground image
    $img_file = __DIR__."/imagenes/certificados_img.jpg";
    $pdf->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
    // restore auto-page-break status
    $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
    // set the starting point for the page content
    $pdf->setPageMark();

    $pdf->SetTextColor(153, 153, 153);        
    $pdf->setFont($fontname, '', 11);
    $pdf->SetXY(220,12);
    $numero = number_pad($estudiante['id'],8);
    $pdf->Cell(70, 0,'N.'.date('Y').'WEB'.$numero,0,1,'L',0,'');
    $pdf->setFont($fontname, '', 9);
    $pdf->StartTransform();
    $pdf->Rotate(+90);
    $pdf->SetXY(0, 10);
    $pdf->Translate(-170, 0);
    $pdf->Cell(0, 0,'Documento firmado digitalmente. Puede ingresar a la siguiente ruta para verificar el certificado',0,1,'L',0,'');
    $pdf->Translate(-15, 0);
    $pdf->Cell(0, 0,'http://escueladeformacion.minambiente.gov.co/certificados/index.php/verificarcertificado '.$codigo,0,1,'L',0,'');
    $pdf->Cell(0, 0,'El documento consta de un total de: 1 pagina/s Pagina 1 de 1',0,1,'L',0,'');
    $pdf->StopTransform();    
    $pdf->setFont($fontname, '', 16);
    $pdf->SetTextColor(79, 74, 74);  
    $pdf->SetXY(13, 96);
    $pdf->writeHTMLCell(0, 0, '', '', utf8_encode($estudiante['nombre']).' '.utf8_encode($estudiante['apellidos']), 0, 0, 0, true, 'C');
    $web_path = '/archivos/certificado'.$estudiante['id'].'.pdf';
    $path =  __DIR__.'/archivos/certificado'.$estudiante['id'].'.pdf';
    $pdf->Output($path, 'F');

    return $web_path;
}

function number_pad($number,$n) { 
    return str_pad((int) $number,$n,"0",STR_PAD_LEFT); 
} 

function buscarCertificado(){
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){  
        $email = $_POST['email'];
        $estudiante = array();
        if (validarEmail($email)) { 
            $estudiante['campo'] = 'valido';
            $conexion = abrirConexionBasededatos();
            $dataEstudiante= mysqli_query($conexion,"SELECT * FROM certificados AS c INNER JOIN estudiantes AS e ON e.id = c.id_estudiante WHERE e.correo ='$email'") or die(mysqli_error($conexion));
            
            $estudiante = mysqli_fetch_array($dataEstudiante, MYSQLI_ASSOC);
            cerrarConexionBasededatos($conexion);
            if ($estudiante == null) {
                $estudiante['success'] =  'false';
            }
            
        } else {
            $estudiante['campo'] = 'novalido';
        }
        $estudiante = verificarCaptcha($estudiante);
        
        return $estudiante;
    }
    else{
        header('Location: ./buscarcertificado');
    }
}


function validarEmail($email){
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function verificarCertificado(){
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){  
        $codigo = $_POST['codigo'];
        $conexion = abrirConexionBasededatos();
        $dataCertificado= mysqli_query($conexion,"SELECT * FROM certificados WHERE codigo ='$codigo'") or die(mysqli_error($conexion));
        $certificado = mysqli_fetch_array($dataCertificado, MYSQLI_ASSOC);
        cerrarConexionBasededatos($conexion);
        if ($certificado == null) {
            $certificado['success'] =  'false';
        }
        $certificado = verificarCaptcha($certificado);
        return $certificado;
    }
    else{
        header('Location: ./verificarcertificado');
    }
}

function verificarCaptcha($responsejson){
    $privatekey = "6LcGgQ4TAAAAAFlcbe1tsA7xiNX9qLREJtoZsAk6";
    if(isset($_POST['g-recaptcha-response'])){
        $captcha=$_POST['g-recaptcha-response'];
    }
    $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$privatekey."&response=".$captcha),TRUE);

    if ($response['success'] === TRUE) {
        $responsejson['captcha'] = 'true';
    }else{            
        $responsejson['captcha'] = 'false';
    }
    return $responsejson;
}
?>
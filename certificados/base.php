<!-- Plantilla base -->
<!DOCTYPE html>
<html>
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo $title ?></title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
    </script>

    <script>
        var rv = -1;  //inicializamos para prevenir fallos de puntero a nulo
        if (navigator.appName == 'Microsoft Internet Explorer')
        {
             var ua = navigator.userAgent;
             var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
             if (re.exec(ua) != null)
                     rv = parseFloat( RegExp.$1 );
             if(rv > -1){
                 switch(rv){
                     case 7.0:
                        
                        alert("La plataforma de la escuela de formaci\u00f3n se encuentra disponible para navegadores desde la versi\u00f3n 8, a continuaci\u00f3n redireccionaremos a una p\u00e1gina que le permitir\u00e1 actualizar su navegador");                      
                        window.location = "http://windows.microsoft.com/es-es/internet-explorer/download-ie";
                        break;
                     case 6.0:
                         alert("La plataforma de la escuela de formaci\u00f3n se encuentra disponible para navegadores desde la versi\u00f3n 8, a continuaci\u00f3n redireccionaremos a una p\u00e1gina que le permitir\u00e1 actualizar su navegador");
                         window.location = "http://windows.microsoft.com/es-es/internet-explorer/download-ie";
                         break;
                     default:
                        
                          break;                 
                 }
             }
        }
    </script>
    <?php if(isset($script)){echo $script;}?>
    <?php if(isset($estilo)){echo $estilo;}?>
     <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>
    <?php echo $contenido ?>
    </body>
</html>

<?php $title = 'Verificar certificado' ?>

<?php ob_start() ?>
 <style>
.btn.btn-primary {
    width: 367px;
    height: 88px;
    background-image: url("../imagenes/btdescargar.png");
    border: none;
}
.btn.btn-default{
	height: 64px;
	width: 59px;
	font-size: 160%;
	background-image: url("../imagenes/btbuscar.png");
}
body{
	background-color: rgb(241,241,241);
}
.container{
	background-image: url("../imagenes/ingresecodigo.jpg");
	width: 846px;
	height: 564px;
	margin-top: 80px;
}
.input-group {
    margin-bottom: 15px;
    margin-top: 210px;
    margin-left: 170px;
    width: 497px;
}
.input-group-btn{
	height: 64px;
	font-size: 160%;
}
.form-control {
	height: 64px;
	font-size: 160%;
}
.boton {
    margin-top: 50px;
    margin-left: 10px;
}
.mensaje{
	font-size: 190%;
	color: white;
}
.descarga{
	margin-top: 30px;
	margin-left: 220px;
}
.error{
	margin-top: 30px;
	margin-left: 205px;
}
#captra{
	position: absolute;
	margin-top: 80px;
	margin-left: 270px;
}
#error2{
	margin-left: 300px;
}
</style>	
<?php $estilo = ob_get_clean() ?>

<?php ob_start() ?>
 <script type="text/javascript">
	var onloadCallback = function() {		
    	grecaptcha.render('captra', {
          'sitekey' : '6LcGgQ4TAAAAAGiGtOetEdAoL5cE6lPlhdhspFrz',
          'theme' : 'dark'
        });        
  	};
 	$(function() {
 		$( ".descarga" ).hide();
 		$( ".error" ).hide();
 		$( "form#formulario" ).submit(function( event ) { 			
		  	event.preventDefault();
		  	console.log($('form#formulario').serialize());
		  	$.post( './verificar', $('form#formulario').serialize(), function(data) {
		  		console.log(data);
		  		if (data.captcha == 'false') {
		  			$( "#error2" ).show();
		  			$( "#error1" ).hide();
		  			$( ".descarga" ).hide();
		  			$('#captra').css({'margin-top':'20px'});
		  			$( "#captra" ).show();
		  			grecaptcha.reset();
		  		}
		  		else{
		  			if (data.success === 'false') {
		  				$( "#error2" ).hide();
			   			$( "#error1" ).show();
			   			$( "#captra" ).show();
			   			$( ".descarga" ).hide();	
			   			$('#captra').css({'margin-top':'20px'});
			   			grecaptcha.reset();
			   		}
			   		else{
			   			$( "#error1" ).hide();
			   			$( "#error2" ).hide();
			   			$( "#captra" ).hide();
			   			$( ".descarga" ).show();
			   		}
		  		}		   		
		  	});
		});
	});
 </script>
<?php $script = ob_get_clean() ?>

<?php ob_start() ?>
<div class="container">
	<form id="formulario" method="post">
	  <div class="input-group">
	    <input type="codigo" class="form-control" id="codigo" name="codigo">
	    <span class="input-group-btn"> 
	    	<button type="submit" class="btn btn-default" type="button"></button> 
	    </span>
	  </div>
	  <div class="descarga">
	  	<p class="mensaje">Tu certifcado ha sido verificado</p>
	  	<div class="fecha">
	  		<p></p>
	  	</div>
	  </div>
	  <div class="error" id="error1">
	  	<p class="mensaje">Tu certifcado no ha sido verficado</p>	
	  </div>
	  <div class="error" id="error2">
	  	<p class="mensaje">Ingresar el captcha</p>	
	  </div>
	  <div id="captra">
	  </div>
	</form>
</div>
<?php $contenido = ob_get_clean() ?>

<?php include 'base.php' ?>

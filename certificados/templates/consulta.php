<?php $title = 'Buscar certificado' ?>

<?php ob_start() ?>
 <style>
.btn.btn-primary {
    width: 367px;
    height: 88px;
    background-image: url("../imagenes/btdescargar.png");
    border: none;
}
.btn.btn-default{
	height: 64px;
	width: 59px;
	font-size: 160%;
	background-image: url("../imagenes/btbuscar.png");
}
body{
	background-color: rgb(241,241,241);
}
.container{
	background-image: url("../imagenes/bgbuscar.jpg");
	width: 846px;
	height: 564px;
	margin-top: 80px;
}
.input-group {
    margin-bottom: 15px;
    margin-top: 210px;
    margin-left: 170px;
    width: 497px;
}
.input-group-btn{
	height: 64px;
	font-size: 160%;
}
.form-control {
	height: 64px;
	font-size: 160%;
}
.boton {
    margin-top: 50px;
    margin-left: 10px;
}
.mensaje{
	font-size: 190%;
	color: white;
}
.descarga{
	margin-top: 30px;
	margin-left: 220px;
}
.error{
	margin-top: 30px;
	margin-left: 205px;
}
#captra{
	position: absolute;
	margin-top: 80px;
	margin-left: 270px;
}
#error2{
	margin-left: 300px;
}
</style>	
<?php $estilo = ob_get_clean() ?>

<?php ob_start() ?>
 <script type="text/javascript">
 	var onloadCallback = function() {		
	    	grecaptcha.render('captra', {
	          'sitekey' : '6LcGgQ4TAAAAAGiGtOetEdAoL5cE6lPlhdhspFrz',
	          'theme' : 'dark'
	        });        
	  	};
 	$(function() { 		
 		$( ".descarga" ).hide();
 		$( ".error" ).hide();
	    $( "form#formulario" ).submit(function( event ) {
		  	$.post( './buscar', $('form#formulario').serialize(), function(data) {
		   		
		  		if (data.captcha == 'false') {
		  			$( "#error2" ).show();
		  			$( "#error1" ).hide();
		  			$( "#error3" ).hide();
		  			$( ".descarga" ).hide();
		  			$('#captra').css({'margin-top':'20px'});
		  			$( "#captra" ).show();
		  			grecaptcha.reset();
		  		}
		  		else{
		  		if (data.campo == 'novalido') {
		   			$( "#error3" ).show();
		   			$( "#error2" ).hide();
		  			$( "#error1" ).hide();
		   			return;
		   		}
		  			if (data.success === 'false') {
		  				$( "#error2" ).hide();
			   			$( "#error1" ).show();
			   			$( "#error3" ).hide();
			   			$( "#captra" ).show();
			   			$( ".descarga" ).hide();	
			   			$('#captra').css({'margin-top':'20px'});
			   			grecaptcha.reset();
			   		}
			   		else{
			   			$( "#error1" ).hide();
			   			$( "#error2" ).hide();
			   			$( "#error3" ).hide();
			   			$( "#captra" ).hide();
			   			$( ".descarga" ).show();
			   			$("#descarga").attr("href", ".."+data.path);
			   		}
		  		}
		  	}, "json");
		  	event.preventDefault();
		});
	});
 </script>
<?php $script = ob_get_clean() ?>

<?php ob_start() ?>
<div class="container">
	<form action="#" method="post" id="formulario">
	  <div class="input-group">
	    <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder=" tucorreo@ejemplo.com">
	    <span class="input-group-btn"> 
	    	<button type="submit" class="btn btn-default" type="button"></button> 
	    </span>
	  </div>
	  <div class="descarga">
	  	<p class="mensaje">Tu certifcado ha sido encontrado</p>
	  	<div class="boton">	  	
	  		<a class="btn btn-primary" id="descarga" download></a>
	  	</div>
	  </div>
	  <div class="error" id="error1">
	  	<p class="mensaje">Tu certifcado no ha sido encontrado</p>	
	  </div>
	  <div class="error" id="error2">
	  	<p class="mensaje">Ingresar el captcha</p>	
	  </div>
	  <div class="error" id="error3">
	  	<p class="mensaje">Dirreción de correo no valida</p>	
	  </div>
	  <div id="captra">
	</form>
</div>
<?php $contenido = ob_get_clean() ?>

<?php include 'base.php' ?>
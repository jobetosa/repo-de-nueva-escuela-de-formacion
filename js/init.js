(function($){


    $('.slider').slider();

    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    if(isIE){
        alert("Por favor actualizar El internet Explore o usar google Chrome para el registro");
        setInterval(function(){
            location.reload();
        },10000)
    }

    $(function(){        
        $('.button-collapse').sideNav();
        $('select').material_select();
        llenardatosjson();

        $('.datepicker').pickadate({  
            max: [2018,12,31],
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 100, // Creates a dropdown of 15 years to control year,
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'OK',
            closeOnSelect: true, // Close upon selecting a date,
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            weekdaysShort: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            formatSubmit: 'dd/mm/yy',
        });

        $("#use_ubi").change(function(){
            console.log($(this).val())
            if($(this).val() == "nac"){
                $(".ubi_nac").show("slow");
                $(".pais").hide("slow");
                //llenardepartamentos();
            }else{
                $(".ubi_nac").hide("slow");
                $(".pais").show("slow");
            }

        });



        $('#use_dep').change(function(){
            Materialize.toast('Selecionaste : '+ $(this).val() +" ahora selecciona la ciudad", 4000);
            llenarciudades($(this).val());
        });

        $('#use_ciu').change(function(){
            Materialize.toast('Selecionaste : '+ $(this).val(), 4000);

        });

        $("#use_vema").focusout(function(){
            if($(this).val() != $("#use_ema").val()){
                Materialize.toast('Email de confirmación No valido. ', 4000);            
            }
        });

        var database = firebase.database();

        var si01 = firebase.database().ref('si01');   
        var pif01 = firebase.database().ref('pif01');   

        si01.on('value', function(snapshot) {
            var c = 80 -Object.keys(snapshot.val()).length;
            $("#si01").text("Seguridad de la Información - "+ c +" Cupos Disponibles ")
            $('#curso').material_select('update');     
            if(c == 0){
                $("#registro").addClass("disabled");
            }
            $("#preload").hide();
        });    

        pif01.on('value', function(snapshot) {
            var c = 80 - Object.keys(snapshot.val()).length;
            $("#pif01").text("Prevención de Incendios forestales - "+ c +" Cupos Disponibles ")
            $('#curso').material_select('update');     
            if(c == 0){
                $("#registro").addClass("disabled");
            }
            $("#preload").hide();
        });    


        $("#use_ndoc").focusout(function(){

            switch($("#curso").val()) {
                case "si01":
                    si01.on('value', function(snapshot) {
                        console.log(snapshot.val());
                        for( n in snapshot.val()){
                            if(n ==  $("#use_ndoc").val() ){
                                Materialize.toast('ya se encuentra registrado en este curso ', 10000);        
                                $("#registro").addClass("disabled");
                                break;
                            }{
                                $("#registro").removeClass("disabled");
                            }
                        }
                    });

                    break;
                case "pif01":
                    pif01.on('value', function(snapshot) {
                        console.log(snapshot.val());
                        for( n in snapshot.val()){
                            if(n ==  $("#use_ndoc").val() ){
                                Materialize.toast('ya se encuentra registrado en este curso ', 10000);            
                                $("#registro").addClass("disabled");
                                break;
                            }{
                                $("#registro").removeClass("disabled");
                            }
                        }
                    });
                    break;
            }
        });



        $("#registro").click(function(){
            if(validacampos()){
                var response = grecaptcha.getResponse();

                if(response.length == 0){ 
                    Materialize.toast('Seleccione el ReCaptcha, <br> por favor', 4000);
                }else{
                    firebase.database().ref($("#curso").val()+"/" + $("#use_ndoc").val()).set({
                        nombres: $("#use_nom").val(),
                        apellidos: $("#use_ape").val(),
                        tipo: $("#use_tdoc").val(),
                        numero: $("#use_ndoc").val(),
                        email:$("#use_ema").val(),
                        email_alterno: $("#use_aema").val(),
                        telefono: $("#use_tel").val(),
                        fecha_nacimiento: $("#use_anio").val(),
                        genero:$("#use_gen").val(),
                        ubicacion: $("#use_ubi").val(),
                        departamento: $("#use_dep").val(),
                        ciudad: $("#use_ciu").val(),
                        pais: $("#use_pais").val(),
                        localidad: $("#use_loc").val(),
                        profesion:  $("#use_pro").val(),
                        empresa: $("#use_emp").val(),

                    });

                    $("#panel").slideUp("slow");    
                    Materialize.toast('FELICIDADES QUEDASTE INSCRITO ', 20000);      
                    $("#use_ndoc").val("");

                    setTimeout(function(){
                        location.reload();
                    },25000)
                }
            }
        });





    }); // end of document ready
})(jQuery); // end of jQuery name space

function llenardatosjson(){
  //  $.getJSON("../base.json", function(data) {
         $.getJSON("//escueladeformacion.minambiente.gov.co/formulario/base.json", function(data) {
        // Aquí puedes manejar la variable data, que contiene el array con los datos del JSON.
        //        console.log(data);

        //console.log(data.Departamentos_con_ciudades[0].departamento)
        for (x in data["Departamentos_con_ciudades"]){
            var value = data.Departamentos_con_ciudades[x].departamento;
            $('#use_dep').append("<option value='"+value+"'>"+value+"</option>");
        }
        for (x in data["paises"]){
            $('#use_pais').append("<option value='"+x+"'>"+x +" - "+data["paises"][x]+"</option>");
        }

        $('#use_dep').material_select('update');
        $('#use_pais').material_select('update');
    });
}

function llenarciudades(dep){
    //$.getJSON("../base.json", function(data) {
    $.getJSON("//escueladeformacion.minambiente.gov.co/formulario/base.json", function(data) {
        // Aquí puedes manejar la variable data, que contiene el array con los datos del JSON.

        $('#use_ciu').html('<option value="" disabled selected>Seleccione ciudad</option>');
        //console.log(data.Departamentos_con_ciudades[0].departamento)
        for (x in data["Departamentos_con_ciudades"]){
            var value = data.Departamentos_con_ciudades[x].departamento;
            if(dep == value){
                var ciudades = data.Departamentos_con_ciudades[x].ciudades
                console.log(ciudades);
                for ( c in ciudades){

                    $('#use_ciu').append("<option value='"+ciudades[c]+"'>"+ciudades[c]+"</option>");
                }
            }

        }

        $('#use_ciu').material_select('update');

    });
}


firebase.auth().signInAnonymously().catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // ...
});


firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        // User is signed in.
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        // ...

        console.log(uid)
    } else {
        // User is signed out.
        // ...
    }
    // ...
});


function validacampos(){

    var campos = [ "nom","ape","tdoc","ndoc","ema","aema","anio","gen","ubi","pro","emp","tel"];

    var ok = true;
    for( x in campos){

        if($("#use_"+campos[x]).val() == null || $("#use_"+campos[x]).val()  == "" ){            
            //            console.log(campos[x]);
            Materialize.toast('Todos los campos son obligatorios ', 4000);    
            $("#use_"+campos[x]).focus()
            //            console.log();
            ok = false;
            break;
        }
    }
    if($("#use_ema").val() != $("#use_vema").val()){
        Materialize.toast('El correo de verificación no coincide.', 4000);    

        ok = false;
    }
    return ok;
}

<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="titulo-cursos"><div class="cpadding"></div>CARTELERA DE CURSOS ACTIVOS</div>
<div class="newsflash<?php echo $moduleclass_sfx; ?>">
<div style="float:left;background-color: #ccc;"><a href="javascript:void(0);" class="lof-previous">Izquierda</a></div>

<div class="contenedorslidej">
<div class="totalcontenslide">
<?php
foreach ($list as $item) :
	require JModuleHelper::getLayoutPath('mod_slider_destacados', '_item');
endforeach;
?>
</div>
</div>

<div style="float:left;background-color: #ccc;"><a href="javascript:void(0);" class="lof-next">Derecha</a></div>
</div>
<script type="text/javascript">
j = jQuery;
var totalelemetnoss= <?php echo count($list) ?>;
var slide_select=0;
var cajas_a_mover=1;
var ancho_caja=270;
var cajas_por_pagina = 4;
var mousesobre=false;


var animacion_activa = false;

j(document).ready(function(){	
	j(".lof-next").click(function(){


		if(animacion_activa == false){
			animacion_activa = true;
			moverDerechasj();

		}
		
	});
	j(".lof-previous").click(function(){

		
		if(animacion_activa == false){

			animacion_activa = true;
			moverIzquierdaj();
		}
		
	});
	j(".newsflash").mouseenter(function(){
		
		mousesobre=true;
	});
	j(".newsflash").mouseleave(function(){
		
		mousesobre=false;
	});
	//setInterval(function(){
		
		//	if(!mousesobre)
		//	moverDerechasj();
		//},3000);
});

	
function moverDerechasj(){
	
			if(slide_select+cajas_por_pagina >= totalelemetnoss ){
				
				jQuery('.totalcontenslide').animate({
				marginLeft:"0px" 	
				},100,function(){
					
							
					slide_select = 0;
					animacion_activa = false;
			});
				
				return;
			}
			jQuery('.totalcontenslide').animate({
				marginLeft:("-"+(cajas_a_mover+slide_select)*ancho_caja)+"px"
				},100,function(){
					
							
					slide_select = slide_select + cajas_a_mover;
					animacion_activa = false;
			});
}

function moverIzquierdaj(){
	
			if(slide_select < 1 ){
				
				jQuery('.totalcontenslide').animate({
				marginLeft:"-"+((totalelemetnoss-cajas_por_pagina)*ancho_caja)+"px" 	
				},100,function(){
					
							
					slide_select = totalelemetnoss-cajas_por_pagina;
					animacion_activa = false;
			});
				
				return;
			}
			jQuery('.totalcontenslide').animate({
				marginLeft:"-"+((slide_select-cajas_a_mover)*ancho_caja)+"px"
				},100,function(){
					
							
					slide_select = slide_select - cajas_a_mover;
					animacion_activa = false;
			});
}


</script>
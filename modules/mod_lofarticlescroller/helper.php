<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: helper.php $Revision
 * @package		modules
 * @subpackage	mod_lofslidenews
 * @copyright	Copyright (C) OCTOBER 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>. All rights reserved.
 * @website 	htt://landofcoder.com
 * @license		GNU General Public License version 2
 */
// no direct access
defined('_JEXEC') or die;
jimport('joomla.filesystem.folder');
require_once JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_content'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'route.php';
if( !defined('PhpThumbFactoryLoaded') ) {
	require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'libs'.DIRECTORY_SEPARATOR.'phpthumb'.DIRECTORY_SEPARATOR.'ThumbLib.inc.php';
	define('PhpThumbFactoryLoaded',1);
}
jimport('joomla.filesystem.folder');
abstract class modLofArticlesSrollerHelper {
	
	
	/**
	 * get list articles
	 */
	public static function getList( $params, $moduleName ){
			return self::getListBySourceName( $params, $moduleName );
	///	}	
	}
		/**
  	 * Get list of articles follow conditions user selected
     * 
     * @param JParameter $params
     * @return array containing list of article
     */ 
	public static function getListBySourceName( &$params, $moduleName ) {
	 	// create thumbnail folder 	

	 	$tmppath = JPATH_SITE.DIRECTORY_SEPARATOR.'cache';//.'lofthumbs';
		$moduleName = 'lofthumbs' ;
	 	$thumbPath = $tmppath.DIRECTORY_SEPARATOR. $moduleName.DIRECTORY_SEPARATOR;
		if( !file_exists($tmppath) ) {
			JFolder::create( $tmppath, 0777 );
		}; 
		if( !file_exists($thumbPath) ) {
			JFolder::create( $thumbPath, 0777 );
		}; 
		$source = 'content';
		// get call object to process getting source
		$path = dirname(__FILE__).DIRECTORY_SEPARATOR."libs".DIRECTORY_SEPARATOR."source".DIRECTORY_SEPARATOR.$source.DIRECTORY_SEPARATOR."source.php";
	
		if( !file_exists($path) ){
			return array();	
		}
		require_once $path;
		$objectName = "LofFree".ucfirst($source)."DataSource";
	 	$object = new $objectName();
	 	$items= $object->setThumbPathInfo($thumbPath, JURI::base()."cache/".$moduleName."/" )
			->setImagesRendered( array( 'thumbnail' => array( (int)$params->get( 'thumbnail_width', 60 ), (int)$params->get( 'thumbnail_height', 60 ))) )
			->getList( $params );
  		return $items;
	}
	/**
	 * load css - javascript file.
	 * 
	 * @param JParameter $params;
	 * @param JModule $module
	 * @return void.
	 */
	public static function loadMediaFiles( $params, $module, $theme='' ){

		$mainframe = JFactory::getApplication();
		$template = self::getTemplate();
		//load style of module
		if(file_exists(JPATH_BASE.DIRECTORY_SEPARATOR.'templates/'.$template.'/css/'.$module->module.'.css')){
			JHTML::stylesheet(  'templates/'.$template.'/css/'.$module->module.'.css' );		
		}			
		// load style of theme follow the setting
		if( $theme && $theme != -1 ){
			$tPath = JPATH_BASE.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.$module->module.'_'.$theme.'.css';
			if( file_exists($tPath) ){
				JHTML::stylesheet( 'templates/'.$template.'/css/'.$module->module.'_'.$theme.'.css');
			} else {
				JHTML::stylesheet('modules/'.$module->module.'/tmpl/'.$theme.'/assets/'.'style.css');	
			}
		} else {
           JHTML::stylesheet( 'modules/'.$module->module.'/assets/'.'style.css' );
		}
		JHTML::script( 'modules/'.$module->module.'/assets/'.'script_12.js');
  	
	}
	
	/**
	 * get name of current template
	 */
	public static function getTemplate(){
		$mainframe = JFactory::getApplication();
		return $mainframe->getTemplate();
	}
	
	/**
	 * Get Layout of the item, if found the overriding layout in the current template, the module will use this file
	 * 
	 * @param string $moduleName is the module's name;
	 * @params string $theme is name of theme choosed
	 * @params string $itemLayoutName is name of item layout.
	 * @return string is full path of the layout
	 */
	public static function getItemLayoutPath($moduleName, $theme ='', $itemLayoutName='_item' ){

		$layout = trim($theme)?trim($theme).DIRECTORY_SEPARATOR.'_item'.DIRECTORY_SEPARATOR.$itemLayoutName:'_item'.DIRECTORY_SEPARATOR.$itemLayoutName;
		if(strpos($layout, ".php")){
             $layout = rtrim($layout, ".php");
        }		
		$path = JModuleHelper::getLayoutPath($moduleName, $layout);	
		if( trim($theme) && !file_exists($path) ){
			// if could not found any item layout in the theme folder, so the module will use the default layout.
			return JModuleHelper::getLayoutPath( $moduleName, '_item'.DIRECTORY_SEPARATOR.$itemLayoutName );
		}
		return $path;
	}
}
?>
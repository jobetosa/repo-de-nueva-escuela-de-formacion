var j = jQuery;

j(document).ready(function(){
	j('.published').insertBefore( "h2" );
	
	j('.registro').click(function(){
	    var popup= j(this).attr('class');
	    var dividir = popup.split(' ');
		j("."+dividir[1]).modal({
			'backdrop':'static',
			'keyboard':false
		});
		//window.setTimeout(function() {
        //window.location.href = 'http://escueladeformacion.minambiente.gov.co/aulamads';
	//}, 3000);
		//return false;
	});
	j('.close').click(function(){
		j(".modal-backdrop.in").remove();
	});

	j('.nspPrev').insertBefore('.nspPagination');
	j('.nspLinks .nspLinkScroll1 ul li').attr('style', 'margin-bottom: 8px !important');
	
	j('button.btn.btn-primary').click(function(){
	
		String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ""); };
		var usr= j('#username').val();
		j('#username').val(usr.trim());
		var pass = j('#password').val();
		j('#password').val(pass.trim());
	
	});
	var $contenido = j(".contenido").hide(),
    $tab = j('#tab'),
    $tab_interna = j('#tab_interna'),
    $panel = j('#panel'),
    $abre_tab = j('a#abre_tab');

    $abre_tab.on('click', function(e) {
    e.preventDefault();
 	});
 	  
 	$tab.toggle(function() {
    $tab.stop().animate({
      right: "0px"
    }, 200, function() {
      $tab_interna.addClass('expandida');
      
    });

    $panel.stop().animate({
      width: "150px",
    }, 200, function() {
      $contenido.fadeIn('fast');
    });
  }, function() {
    $contenido.fadeOut('fast', function() {
      $tab.stop().animate({
        right: "-150px"
      }, 200, function() {
        $tab_interna.removeClass();
      });
      $panel.stop().animate({
        width: "0",
      }, 200);
    });
  });
});
<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js');

// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css');

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
	$span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span9";
}
else
{
	$span = "span12";
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle')) . '</span>';
}
else
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	
	<!-- Latest compiled and minified JavaScript -->
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<?php // Use of Google Font ?>
	<?php if ($this->params->get('googleFont')) : ?>
		<link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
		<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName')); ?>', sans-serif;
			}
		</style>
	<?php endif; ?>
	<?php // Template color ?>
	<?php if ($this->params->get('templateColor')) : ?>
		
	<style type="text/css">
		body.site
		{
			border-top: 3px solid <?php echo $this->params->get('templateColor'); ?>;
			background-color: <?php echo $this->params->get('templateBackgroundColor'); ?>
		}
		a
		{
			color: <?php echo $this->params->get('templateColor'); ?>;
		}
		.navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
		.btn-primary
		{
			background: <?php echo $this->params->get('templateColor'); ?>;
		}
		.navbar-inner
		{
			-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
		}
	</style>
	<?php endif; ?>
	<!--[if lt IE 9]>
		<script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '');
?>">

	<!-- Body -->
	<div class="body">
		<div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
			<!-- Header -->
			<div class="logos-header">
 					 <div class="col-md-6 logo-izq"><img src="http://portalappdes.minambiente.gov.co/cienagagrande/images/recursos/logo-cienaga.png" alt="logo1" /></div>
 				     <div class="col-md-6 logo-izq">
 				     	<a href="https://www.minambiente.gov.co/" target="_blank"><img src="http://portalappdes.minambiente.gov.co/cienagagrande/images/recursos/logomin.jpg" alt="logo2" /></a>
 				     		<div class="redes-sociales">
								<ul>
									<li><a href="https://plus.google.com/109223660506763562157" target="_blank" rel="publisher" title="Google+"><img src="https://www.minambiente.gov.co/images/social-google-plus-img.png" alt="Google+" width="20" height="20"></a></li>
									<li><a href="https://twitter.com/minambienteco" target="_blank" title="Twitter"><img src="https://www.minambiente.gov.co/images/social-tw-img.png" alt="Twitter" width="20" height="20"></a></li>
									<li><a href="https://www.facebook.com/pages/Ministerio-de-Ambiente-y-Desarrollo-Sostenible/118961431546715" target="_blank" title="Facebook"><img src="https://www.minambiente.gov.co/images/social-fb-img.png" alt="Facebook" width="20" height="20"></a></li>
									<li><a href="http://www.youtube.com/user/minambientegov" target="_blank" title="Youtube"><img src="https://www.minambiente.gov.co/images/social-yt-img.png" alt="Youtube" width="20" height="20"></a></li>
									<li><a href="http://instagram.com/minambientecol" target="_blank" title="Consulte el Instagram"><img src="https://www.minambiente.gov.co/images/instagram.png" alt="Consulte su correo institucional" width="20" height="20"></a></li>
									<li><a href="https://correo.minambiente.gov.co/owa" target="_blank" title="Consulte su correo institucional"><img src="https://www.minambiente.gov.co/images/social-correo-institucional-img.png" alt="Consulte su correo institucional" width="20" height="20"></a></li>
								</ul>
							</div>
 				     </div>
			</div>
			<div class="contenedor-banner">
				<div class="col-md-12 banner"><jdoc:include type="modules" name="banner" style="xhtml" /></div>
			</div>
			<div class="contenedor-menu">
				<div class="col-md-12 menu">
					<jdoc:include type="modules" name="menu" style="xhtml" />
				</div>
			</div>
			<div class="contenedor-articulo">
				<div style="margin-bottom: 30px;" class="col-md-8 table-responsive">
				<jdoc:include type="message" />
				<jdoc:include type="component" />
				</div>
				<div class="col-md-4 cont-event">
				<jdoc:include type="modules" name="ultimos-eventos" style="xhtml" />
				</div>
			</div>
          <div class="contenedor-slider col-md-12">
			<div class="slider">
				<jdoc:include type="modules" name="slider" style="xhtml" />
			</div>	
			</div>
			
			
		</div>
	</div>
	<!-- Footer -->
	<footer class="footer" role="contentinfo">
		<div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
			<hr />
			<jdoc:include type="modules" name="footer" style="none" />
			<div class="pie">
			<p>Ministerio de Ambiente y Desarrollo Sostenible de la República de Colombia</p>
			<p>Líneas gratuitas 018000915060 - 018000919301</p>
			<p>Calle 37 No. 8-40 - Conmutador: (57-1) 3323400 Horario de atención: Lunes a Viernes de 08:00 a.m. - 4:30 p.m</p>
			</div>
		</div>
		<p class="pull-right">
				<a href="#top" id="back-top">
					<?php echo JText::_('TPL_PROTOSTAR_BACKTOTOP'); ?>
				</a>
			</p>
	</footer>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
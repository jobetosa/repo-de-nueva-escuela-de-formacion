<?php 
$this->setGenerator(null);
$iduserax= JFactory::getUser()->id;
if(!empty($iduserax)){
	$app = JFactory::getApplication();
$app->redirect('http://escueladeformacion.minambiente.gov.co/index.php/inicio', false);
return;
}



?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="<?php echo $this->language; ?>"
	lang="<?php echo $this->language; ?>"
	dir="<?php echo $this->direction; ?>">
<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-11686645-4', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
var rv = -1;  //inicializamos para prevenir fallos de puntero a nulo
if (navigator.appName == 'Microsoft Internet Explorer')
{
     var ua = navigator.userAgent;
     var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
     if (re.exec(ua) != null)
              rv = parseFloat( RegExp.$1 );
     if(rv > -1){
         switch(rv){
             case 7.0:
                 
               	alert("La plataforma de la escuela de formaci\u00f3n se encuentra disponible para navegadores desde la versi\u00f3n 8, a continuaci\u00f3n redireccionaremos a una p\u00e1gina que le permitir\u00e1 actualizar su navegador, para visualizar los m\u00f3dulos del curso de cambio clim\u00e1tico");           	
               	window.location = "http://windows.microsoft.com/es-es/internet-explorer/download-ie";
                break;
             case 6.0: 
            	 alert("La plataforma de la escuela de formaci\u00f3n se encuentra disponible para navegadores desde la versi\u00f3n 8, a continuaci\u00f3n redireccionaremos a una p\u00e1gina que le permitir\u00e1 actualizar su navegador, para visualizar los m\u00f3dulos del curso de cambio clim\u00e1tico");
                 window.location = "http://windows.microsoft.com/es-es/internet-explorer/download-ie";
                 break;
             default:
                 
                  break;                  
         }
     }
}
</script>
     
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Login Escuela de Formaci&oacute;n</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon-minis.ico" />
<link rel="stylesheet"
	href="<?php echo $this->baseurl . '/templates/' . $this->template."/css/template.css"  ?>"
	type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<jdoc:include type="head" />
</head>

<body>

	<div class="tpl-page">
		<div class="tpl-content-general">
        <div class="iconos-gobierno">
		<jdoc:include type="modules" name="escudo-gobierno" style="none" />
		</div>
			<div class="formulario">
				<jdoc:include type="message" />
				<jdoc:include type="component" />
			</div>
		</div>
	</div>
</body>
</html>

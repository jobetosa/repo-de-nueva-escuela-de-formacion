<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template."/css/template.css"  ?>" type="text/css">	   
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>	   
	<script src="<?php echo $this->baseurl . '/templates/' . $this->template."/js/template.js"  ?>" type="text/javascript"></script>	   
	<jdoc:include type="head" />
</head>
<body>
	<jdoc:include type="message" />
	<jdoc:include type="component" />						
</body>
</html>

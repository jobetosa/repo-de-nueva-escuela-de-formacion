<?php
$this->setGenerator(null);
$iduserax= JFactory::getUser()->id;
if(!empty($iduserax)){
    
}


/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */



// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;


// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
    $fullWidth = 1;
}
else
{
    $fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/script.js', 'text/javascript');
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/estilosgenerales.css');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
    $span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
    $span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
    $span = "span9";
}
else
{
    $span = "span12";
}


?>

<!DOCTYPE html>
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>"><head><meta charset="gb18030">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />  
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-11686645-4', 'auto');
        ga('send', 'pageview');


    </script>

    <jdoc:include type="head" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <?php
    // Use of Google Font
    if ($this->params->get('googleFont')) 
    {
    ?>

    <?php
    }
    ?>
    <?php
    // Template color
    if ($this->params->get('templateColor'))
    {
    ?>

    <?php
    }
    ?>

    </head>


    <body class="site <?php echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '');
                 ?>">

        <!-- Body -->


        <div class="oculta-contenido">
            <div class="body">
                <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : '');?>">
                    <!-- Header -->
                    <div class="header">
                        <div class="header-inner clearfix">
                            <div id="content_cab">
                                <div id="fecha-hora"><span id="ciudadyfecha"></span><script src="templates/escueladeformacion-home/js/fechahora.js" type="text/javascript"></script><noscript>Fecha actual</noscript>
                                    <jdoc:include type="modules" name="contador" style="none" />
                                </div>
                                <div id="logo"><a href="//www.minambiente.gov.co"><img style=" margin-top:16px; margin-left:3% ;"width="490" height="82" title="Ministerio de Ambiente y Desarrollo Sostenible de la República de Colombia" alt="Ministerio de Ambiente y Desarrollo Sostenible de la República de Colombia" src="//escueladeformacion.minambiente.gov.co/images/logos/escudo-ministerio.png"></a></div>
                                <div id="redes-der">
                                    <div id="traductor"><jdoc:include type="modules" name="traductor" style="none" /></div>
                                    <div id="buscador"><jdoc:include type="modules" name="buscador" style="none" /></div>
                                    <div id="redes-sociales">
                                        <div id="plantilla-redes">
                                            <jdoc:include type="modules" name="redes-sociales" style="none" />
                                        </div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <jdoc:include type="message" />         
                    
                    <div id="menu">
                        <jdoc:include type="modules" name="menu" style="none" />
                    </div>

                    <div id="banner">
                        <jdoc:include type="modules" name="banner" style="none" />
                    </div>

                    <div id="cursos">
                        <jdoc:include type="modules" name="cursos-abiertos" style="none" />
                         <jdoc:include type="modules" name="cursos-cerrados" style="none" />
                    </div>
                    <div class="noticias">
                    	<div class="titulo-cursos"><div style="margin-left: 33px;" class="cpadding" wfd-id="60"></div>NOVEDADES DE LA ESCUELA VIRTUAL</div>
                        <jdoc:include type="modules" name="noticias" style="none" />
                    </div>
                
                      
                            <div class="contenedor-articulo">
                				<jdoc:include type="component" />
                			</div>

                            <div id="novedades">
                                <jdoc:include type="modules" name="novedades" style="none" />
                            </div>
                            <a href="#" class="visible-md visible-lg" id="abre_tab">  
                                <div id="tab"> 
                                    <div id="tab_interna">
                                        <img src="https://tuwebcompany.com/portalmads/wp-content/uploads/2019/07/chat.png" />	
                                    </div> 
                                </div> 
                            </a>  
                            <!-- Panel oculto -->  
                            <div id="panel">  
                                <div class="contenido">
                                    <a target="_blank" href="mailto:aulamads@minambiente.gov.co"><img src="https://tuwebcompany.com/escuela/images/rediseno/chat.png" /></a>
                                </div>
                            </div>  
                            <div>
                            </div> 
                   

                    <div id="modal_registro" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">

                                </div>
                                <div class="modal-body">

                                    <div class="formulario" id="myModal">
                                        <h4 style="text-align:center;" class="modal-title">Para acceder a estos talleres se requiere previa convocatoria, inscripción y selección.</h4>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <?php if ($this->countModules('position-1')) : ?>
            <div class="navigation">
                <jdoc:include type="modules" name="position-1" style="none" />
            </div>
            <?php endif; ?>


            <!-- Footer -->
            <div class="footer">
                <div class="<?php echo ($params->get('fluidContainer') ? '-fluid' : '');?>">
                    <div id="pie">
                        <jdoc:include type="modules" name="pie" style="none" />
                    </div>

                </div>
            </div>	
        </div>
    </body>
</html>

//Javacritpt Modulo mod_lofarticlescroller

if( typeof(jQuery) != 'undefined' ){
	jQuery.noConflict();
}
if( typeof(LofSlideshow) == 'undefined' ){
	var LofSlideshow = new Class( {
		initialize:function( _lofmain, options ){
			this.setting = Object.append({
				autoStart			: true,
				descStyle	    	: 'sliding',
				mainItemSelector    : 'div.lof-main-item',
				navSelector  		: 'li' ,
				navigatorEvent		: 'click',
				interval	  	 	:  2000,
				auto			    : false,
				navItemsDisplay		: 3,
				startItem			: 0,
				navItemHeight		: 100,
				navItemWidth 		: 310
			}, options || {} );
			
			var eMain 	   = _lofmain.getElement('.lof-main-wapper');
			var eNavigator = _lofmain.getElement('.lof-navigator-outer .lof-navigator');
			var eNavOuter  = _lofmain.getElement('.lof-navigator-outer');	  
			this.currentNo  = 0;
			this.nextNo     = null;
			this.previousNo = null;
			this.fxItems	= [];	
			this.minSize 	= 0;
			this.onClick = false;
			if( eMain != null ){
				this.slides	   = eMain.getElements( this.setting.mainItemSelector );
				this.maxWidth  = eMain.getStyle('width').toInt();
				this.maxHeight = eMain.getStyle('height').toInt();
				this.styleMode = this.__getStyleMode();  
				var fx =  Object.append({waiting:false, onComplete:function(){ this.onClick=false, this.slides.setStyle("z-index",0); this.slides[this.currentNo].setStyle("z-index",3) }.bind(this) }, this.setting.fxObject );
				this.slides.each( function(item, index) {	
					item.setStyles( eval('({"'+this.styleMode[0]+'": index * this.maxSize,"'+this.styleMode[1]+'":Math.abs(this.maxSize),"display" : "block"})') );		
					this.fxItems[index] = new Fx.Morph( item,  fx );
				}.bind(this) );
				if( this.styleMode[0] == 'opacity' || this.styleMode[0] =='z-index' ){
					this.slides[0].setStyle(this.styleMode[0],'1');
				}
				eMain.addEvents( { 'mouseenter' : this.stop.bind(this),
							   	   'mouseleave' :function(e){ 
								    if(  this.setting.auto ){
										this.play( this.setting.interval,'next', true );
									} }.bind(this) } );
			}
			if( (eNavigator != null) && (eNavOuter!= null) ){
				var modes = {
					horizontal : ['margin-left', 'width', 'height', 'navItemWidth', 'navItemHeight'],
					vertical   : ['top', 'height', 'width', 'navItemHeight', 'navItemWidth']
				}
				var mode = ( this.setting.navPos == 'bottom' || this.setting.navPos == 'top' )?'horizontal' : 'vertical';	
		
				this.navigatorItems = eNavigator.getElements( this.setting.navSelector );
				if( this.setting.navItemsDisplay > this.navigatorItems.length ){
					this.setting.navItemsDisplay = this.navigatorItems.length;	
				}
				
				if( _lofmain.getElement(".lof-bullets") != null  ){
					this.setting.navItemHeight = this.navigatorItems[0].offsetHeight;
					this.setting.navItemWidth  = this.navigatorItems[0].offsetWidth;
				}
				
				this.navigatorSlideSize = this.setting[modes[mode][3]];	 
				eNavOuter.setStyle(modes[mode][1], this.setting.navItemsDisplay * this.navigatorSlideSize);
                eNavOuter.setStyle(modes[mode][2], this.setting[modes[mode][4]] );
								
				this.navigatorMode = 	modes[mode][0];		
				this.navigatorFx = new Fx.Tween( eNavigator,{transition:Fx.Transitions.Sine.easeInOut,duration:900} );
					
					
				 if(  this.setting.auto ){
				//	this.registerMousewheelHandler( eNavigator ); // allow to use the srcoll
				 }
				this.navigatorItems.each( function(item,index) {
					item.addEvent( this.setting.navigatorEvent, function(){		
					if( this.onClick ) return ;
						this.jumping( index, true );
						this.setNavActive( index, item );	
					}.bind(this) ); 
	
						item.setStyles( { 'height' : this.setting.navItemHeight,
									  	  'width'  : this.setting.navItemWidth} );		
				}.bind(this) );
				// set default setting
				this.currentNo=this.setting.startItem.toInt()>this.slides.length?this.slides.length:this.setting.startItem.toInt();
				this.setNavActive( this.currentNo );
				this.slides.setStyle(this.styleMode[0] ,this.maxSize ).setStyle("z-index",0);
				this.slides[this.currentNo].setStyle(this.styleMode[0] ,this.minSize ).setStyle("z-index",3);
	
			}
		},
		navivationAnimate:function( currentIndex ) { 
			if (currentIndex <= this.setting.startItem 
				|| currentIndex - this.setting.startItem >= this.setting.navItemsDisplay-1) {
					this.setting.startItem = currentIndex - this.setting.navItemsDisplay+2;
					if (this.setting.startItem < 0) this.setting.startItem = 0;
					if (this.setting.startItem >this.slides.length-this.setting.navItemsDisplay) {
						this.setting.startItem = this.slides.length-this.setting.navItemsDisplay;
					}
			}		
			this.navigatorFx.cancel().start( this.navigatorMode,-this.setting.startItem*this.navigatorSlideSize );	
		},
		setNavActive:function( index, item ){
			if( (this.navigatorItems != null ) && ( this.navigatorItems[index] != null ) ){ 
				this.navigatorItems.removeClass('active');
				this.navigatorItems[index].addClass('active');	
				this.navivationAnimate( this.currentNo );	
			}
		},
		__getStyleMode:function(){
			switch( this.setting.direction ){
				case 'opacity': this.maxSize=0; this.minSize=1; return ['opacity','opacity'];
				case 'replace': this.maxSize=0; this.minSize=1; return ['display','display'];
				case 'vrup':    this.maxSize=this.maxHeight;    return ['top','height'];
				case 'vrdown':  this.maxSize=-this.maxHeight;   return ['top','height'];
				case 'hrright': this.maxSize=-this.maxWidth;    return ['left','width'];
				case 'hrleft':
				default: this.maxSize=this.maxWidth; return ['left','width'];
			}
		},
		registerMousewheelHandler:function( element ){ 
			element.addEvents({
				'wheelup': function(e) {
					
					e = new Event(e).cancel(); 
						this.previous(true);
				}.bind(this),
			 
				'wheeldown': function(e) {
					e = new Event(e).cancel();
				
					this.next(true);
				}.bind(this)
			} );
		},
		registerButtonsControl:function( eventHandler, objects, isHover ){
			if( (objects != null ) && this.slides.length > 1 ){
				for( var action in objects ){ 
					if( (this[action.toString()] != null )  && (objects[action] != null ) ){
						objects[action].addEvent( eventHandler, this[action.toString()].bind(this, true) );
					}
				}
			}
			return this;	
		},
		start:function( isStart, obj ){
			this.setting.auto = isStart;
			// if use the preload image.
			if( obj ) {
				this.preloadImages(  obj );
			} else {
				if( this.setting.auto && this.slides.length > 1 ){
						this.play( this.setting.interval,'next', true );}	
			}
		},
		onComplete:function( obj ){
			(function(){																
				obj.fade('out');		
			}).delay(500);
			if( this.setting.auto && this.slides.length > 1 ){
				this.play( this.setting.interval,'next', true );}	
			
		},
		preloadImages:function( obj ){  
			var loaded=[];
			var counter=0;
			var self = this;
			var _length = this.slides.getElements('img').length;
			this.timer = setInterval( function(){
				if(counter >= _length) {	
						$clear(self.timer);
						self.onComplete( obj );
						return true;
				} 
			}, 200);
			this.slides.getElements('img').each( function(img, index){
				image = new Image();
				image.src=img.src;
				if( !image.complete ){				  
					image.onload =function(){
						counter++;
					}
					image.onerror =function(){ 
						counter++;
					}
				}else {
					counter++;
				}
			} );
		},
		onProcessing:function( manual, start, end ){	
			this.onClick = true;
			this.previousNo = this.currentNo + (this.currentNo>0 ? -1 : this.slides.length-1);
			this.nextNo 	= this.currentNo + (this.currentNo < this.slides.length-1 ? 1 : 1- this.slides.length);				
			return this;
		},
		finishFx:function( manual ){
			if( manual ) this.stop();
			if( manual && this.setting.auto ){	
				this.play( this.setting.interval,'next', true );
			}		
			this.setNavActive( this.currentNo );	
		},
		getObjectDirection:function( start, end ){
			return eval("({'"+this.styleMode[0]+"':["+start+", "+end+"]})");	
		},
		fxStart:function( index, obj ){
			this.fxItems[index].cancel(true, false).start( obj );
			return this;
		},
		jumping:function( no, manual ){
			this.stop();
			if( this.currentNo == no ) return;
			var object = this.onProcessing( null, manual, 0, this.maxSize );
			if( this.currentNo < no  ){
				object.fxStart( no, this.getObjectDirection(this.maxSize , this.minSize) );
				object.fxStart( this.currentNo, this.getObjectDirection(this.minSize,  -this.maxSize) );
			} else {
				object.fxStart( no, this.getObjectDirection(-this.maxSize , this.minSize) );
				object.fxStart( this.currentNo, this.getObjectDirection(this.minSize,  this.maxSize) );	
			}
			object.finishFx( manual );	
			this.currentNo  = no;
		},
		next:function( manual , item){
			if( this.onClick ) return ;
			this.currentNo += (this.currentNo < this.slides.length-1) ? 1 : (1 - this.slides.length);	
			this.onProcessing( item, manual, 0, this.maxSize )
				.fxStart( this.currentNo, this.getObjectDirection(this.maxSize ,this.minSize) )
				.fxStart( this.previousNo, this.getObjectDirection(this.minSize, -this.maxSize) )
				.finishFx( manual );
		},
		previous:function( manual, item ){
			if( this.onClick ) return ;
			this.currentNo += this.currentNo > 0 ? -1 : this.slides.length - 1;
			this.onProcessing( item, manual, -this.maxWidth, this.minSize )
				.fxStart( this.nextNo, this.getObjectDirection(this.minSize, this.maxSize) )
				.fxStart( this.currentNo,  this.getObjectDirection(-this.maxSize, this.minSize) )
				.finishFx( manual	);			
		},
		play:function( delay, direction, wait ){
			this.stop(); 
			if(!wait){ this[direction](false); }
			this.isRun = this[direction].periodical(delay,this,true);
		},stop:function(){  clearTimeout(this.isRun ); clearInterval(this.isRun); }
	} );
}






//Javacritpt Modulo mod_news_pro_gk5

var NSP5 = new Class({
	// class fields
	animation: true,
	anim_arts: false,
	anim_lists: false,
	arts: null,
	arts_block_width: 0,
	arts_current: 0,
	arts_pages: null,
	arts_per_page: null,
	arts_scroller: null,
	config: null,
	hover_anim: null,
	links: null,
	links_block_width: 0,
	links_pages: null,
	links_pages_amount: null,
	links_scroller: null,
	links_current: 0,
	modInterface: null,
	module: null,
	// touch events properties
	swipe_max_time: 500, // in ms
	swipe_min_move: 30, // in px
	//
	initialize: function(module) {
		// init class fields
		this.init_fields(module);
		// init the interface
		this.init_interface();
	},
	init_fields: function(module) {
		// the most important class field ;)
		this.module = module;
		this.module.addClass('activated');
		// rest of the fields
		this.config = JSON.decode(this.module.get('data-config'));
		this.config['animation_function'] = eval(this.config['animation_function']);
		this.arts = this.module.getElements('.nspArt');
		this.arts_pages = this.module.getElements('.nspArtPage');
		this.arts_per_page = this.config['news_column'] * this.config['news_rows'];
		this.hover_anim = this.module.hasClass('hover');
		this.links = (this.module.getElement('.nspLinkScroll1')) ? this.module.getElement('.nspLinkScroll1').getElements('li') : [];
		this.links_pages = this.module.getElements('.nspList');
		this.links_pages_amount = Math.ceil(Math.ceil(this.links.length / this.config['links_amount']) / this.config['links_columns_amount']);
		this.modInterface = { 
			top: this.module.getElement('.nspTopInterface'), 
			bottom: this.module.getElement('.nspBotInterface')
		};
		this.pages_amount = Math.ceil(this.arts.length / this.arts_per_page);
	},
	init_interface: function() {
		var $this = this;
		// arts
		if(this.arts.length > 0){
			this.arts_block_width = 100;
			
			this.arts_scroller = new Fx.Tween($this.module.getElement('.nspArtScroll2'), {
				duration: $this.config['animation_speed'], 
				wait:false, 
				property: 'margin-left', 
				unit: '%',
				transition: $this.config['animation_function']
			});
		}
		// events
		this.module.addEvents({
			'mouseenter': function() {
				if(!$this.module.hasClass('onhover')) $this.module.addClass('onhover');
			},
			'mouseleave': function() {
				if($this.module.hasClass('onhover')) $this.module.removeClass('onhover');
			}
		});
		// links
		if(this.links.length > 0){
			this.links_block_width = 100;
			
			this.links_scroller = new Fx.Tween($this.module.getElement('.nspLinkScroll2'), {
				duration:$this.config['animation_speed'], 
				wait:false, 
				property: 'margin-left',
				unit: '%',
				transition: $this.config['animation_function']
			});
		}
		// top interface
		this.nsp_art_list(0, 'top');
		this.nsp_art_list(0, 'bottom');
		//
		if(this.modInterface.top && this.modInterface.top.getElement('.nspPagination')){
			this.modInterface.top.getElement('.nspPagination').getElements('li').each(function(item,i){
				item.addEvent($this.hover_anim ? 'mouseenter' : 'click', function(){
					$this.arts_anim(i);
				});	
			});
		}
		//
		if(this.modInterface.top && this.modInterface.top.getElement('.nspPrev')){
			this.modInterface.top.getElement('.nspPrev').addEvent("click", function(){
				$this.arts_anim('prev');
			});
			
			this.modInterface.top.getElement('.nspNext').addEvent("click", function(){
				$this.arts_anim('next');
			});
		}
		// bottom interface
		if(this.modInterface.bottom && this.modInterface.bottom.getElement('.nspPagination')){
			this.modInterface.bottom.getElement('.nspPagination').getElements('li').each(function(item,i){
				item.addEvent($this.hover_anim ? 'mouseenter' : 'click', function(){	
					$this.lists_anim(i);
				});	
			});
		}
		//
		if(this.modInterface.bottom && this.modInterface.bottom.getElement('.nspPrev')){
			this.modInterface.bottom.getElement('.nspPrev').addEvent("click", function(){
				$this.lists_anim('prev');
			});
			
			this.modInterface.bottom.getElement('.nspNext').addEvent("click", function(){
				$this.lists_anim('next');
			});
		}
		//
		if(this.module.hasClass('autoanim')){
			(function(){
				$this.nsp_gk5_autoanim();
			}).delay($this.config['animation_interval']);
		}
		// Touch events for the articles
		var arts_wrap = this.module.getElement('.nspArts');
		if(arts_wrap) {		
			var arts_pos_start_x = 0;
			var arts_pos_start_y = 0;
			var arts_time_start = 0;
			var arts_swipe = false;
			
			arts_wrap.addEvent('touchstart', function(e) {
				arts_swipe = true;
				
				if(e.changedTouches.length > 0) {
					arts_pos_start_x = e.changedTouches[0].pageX;
					arts_pos_start_y = e.changedTouches[0].pageY;
					arts_time_start = new Date().getTime();
				}
			});
			
			arts_wrap.addEvent('touchmove', function(e) {
				if(e.changedTouches.length > 0 && arts_swipe) {
					if(
						Math.abs(e.changedTouches[0].pageX - arts_pos_start_x) > Math.abs(e.changedTouches[0].pageY - arts_pos_start_y)
					) {
						e.preventDefault();
					} else {
						arts_swipe = false;
					}
				}
			});
			
			arts_wrap.addEvent('touchend', function(e) {
				if(e.changedTouches.length > 0 && arts_swipe) {					
					if(
						Math.abs(e.changedTouches[0].pageX - arts_pos_start_x) >= $this.swipe_min_move && 
						new Date().getTime() - arts_time_start <= $this.swipe_max_time
					) {
						if(e.changedTouches[0].pageX - arts_pos_start_x > 0) {
							$this.arts_anim('prev');
						} else {
							$this.arts_anim('next');
						}
					}
				}
			});
		}
		// Touch events for the links
		var links_wrap = this.module.getElement('.nspLinksWrap');
		if(links_wrap) {
			var links_pos_start_x = 0;
			var links_pos_start_y = 0;
			var links_time_start = 0;
			var links_swipe = false;
			
			links_wrap.addEvent('touchstart', function(e) {
				links_swipe = true;
				
				if(e.changedTouches.length > 0) {
					links_pos_start_x = e.changedTouches[0].pageX;
					links_pos_start_y = e.changedTouches[0].pageY;
					links_time_start = new Date().getTime();
				}
			});
			
			links_wrap.addEvent('touchmove', function(e) {
				if(e.changedTouches.length > 0 && links_swipe) {
					if(
						Math.abs(e.changedTouches[0].pageX - links_pos_start_x) > Math.abs(e.changedTouches[0].pageY - links_pos_start_y)
					) {
						e.preventDefault();
					} else {
						links_swipe = false;
					}
				}
			});
			
			links_wrap.addEvent('touchend', function(e) {
				if(e.changedTouches.length > 0 && links_swipe) {					
					if(
						Math.abs(e.changedTouches[0].pageX - links_pos_start_x) >= $this.swipe_min_move && 
						new Date().getTime() - links_time_start <= $this.swipe_max_time
					) {
						if(e.changedTouches[0].pageX - links_pos_start_x > 0) {
							$this.lists_anim('prev');
						} else {
							$this.lists_anim('next');
						}
					}
				}
			});
		}
	},
	//
	nsp_art_list: function(i, pos){
		var num  = (i !== null) ? i : (pos == 'top') ? this.arts_current : this.links_current;
		
		if(this.modInterface[pos] && this.modInterface[pos].getElement('.nspPagination')){
			var pagination = this.modInterface[pos].getElement('.nspPagination');
			pagination.getElements('li').setProperty('class', '');
			pagination.getElements('li')[num].setProperty('class', 'active');
		}
	},
	//
	arts_anim: function(dir) {
		if(!this.anim_arts) {
			var $this = this;
			this.anim_arts = true;
			this.arts_pages[this.arts_current].removeClass('active');
			
			if(dir == 'next') {
				this.arts_current = (this.arts_current == this.pages_amount - 1) ? 0 : this.arts_current + 1;
			} else if(dir == 'prev') {
				this.arts_current = (this.arts_current == 0) ? this.pages_amount - 1 : this.arts_current - 1;
			} else {
				this.arts_current = dir;
			}
			
			this.arts_scroller.start(-1 * this.arts_current * this.arts_block_width);
			
			(function() {
				$this.arts_pages[$this.arts_current].addClass('active');
			}).delay(this.config['animation_speed'] * 0.5);
			
			(function() {
				$this.anim_arts = false;
			}).delay(this.config['animation_speed']);
			
			this.nsp_art_list(this.arts_current, 'top');
			this.animation = false;
			(function(){
				$this.animation = true;
			}).delay(this.config['animation_interval'] * 0.8);
		}
	},
	//
	lists_anim: function(dir) {
		if(!this.anim_lists) {
			var $this = this;
			this.anim_lists = true;
			
			for(var x = 0; x < this.config['links_columns_amount']; x++) {
				var item = this.links_pages[this.links_current * this.config['links_columns_amount'] + x];
				if(item) item.removeClass('active');
			}
			
			if(dir == 'next') {
				this.links_current = (this.links_current == this.links_pages_amount - 1) ? 0 : this.links_current + 1;
			} else if(dir == 'prev') {
				this.links_current = (this.links_current == 0) ? this.links_pages_amount - 1 : this.links_current - 1;
			} else {
				$this.links_current = dir;
			}
			
			(function() {
				$this.anim_lists = false;
			}).delay(this.config['animation_speed']);
			
			(function() {
				for(var x = 0; x < $this.config['links_columns_amount']; x++) {
					var item = $this.links_pages[$this.links_current * $this.config['links_columns_amount'] + x]; 
					if(item) item.addClass('active');
				}
			}).delay(this.config['animation_speed'] * 0.5);
			
			this.links_scroller.start(-1 * this.links_current * this.links_block_width);
			this.nsp_art_list(null, 'bottom');
		}
	},
	//
	nsp_gk5_autoanim: function() {
		var $this = this;
		//
		if(!this.module.hasClass('onhover')) {
			this.arts_anim('next');
		}
		//
		(function() {
			$this.nsp_gk5_autoanim();
		}).delay($this.config['animation_interval']);
	}
});
//
window.addEvent("load", function(){	
	$$('.nspMain').each(function(module){	
		if(!module.hasClass('activated')) {	
			new NSP5(module);
		}
	});
});










//Javacritpt Modulo mod_slideshowck


jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		//alert(jQuery.easing.default);
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158; 
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});

//Javacritpt Modulo mod_slideshowck

(function(e){e.fn.camera=function(t,n){function s(){if(navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPod/i)){return true}}function I(){var t=e(b).width();e("li",b).removeClass("camera_visThumb");e("li",b).each(function(){var n=e(this).position(),r=e("ul",b).outerWidth(),i=e("ul",b).offset().left,s=e("> div",b).offset().left,o=s-i;e(".camera_prevThumbs",nt).removeClass("hideNav").css("visibility","visible");e(".camera_nextThumbs",nt).removeClass("hideNav").css("visibility","visible");var u=n.left,a=n.left+e(this).width();if(a-o<=t&&u-o>=0){e(this).addClass("camera_visThumb")}})}function U(){if(t.lightbox=="mediaboxck"&&typeof Mediabox!="undefined"){Mediabox.scanPage()}else if(t.lightbox=="squeezebox"){SqueezeBox.initialize({});SqueezeBox.assign($$("a.camera_link[rel=lightbox]"),{})}}function z(){w=o.width();if(t.height.indexOf("%")!=-1){var e=Math.round(w/(100/parseFloat(t.height)));if(t.minHeight!=""&&e<parseFloat(t.minHeight)){E=parseFloat(t.minHeight)}else{E=e}o.css({height:E})}else if(t.height=="auto"){E=o.height()}else{E=parseFloat(t.height);o.css({height:E})}}function W(){function r(){w=o.width();z();e(".camerarelative",c).css({width:w,height:E});e(".imgLoaded",c).each(function(){var n=e(this),r=n.attr("width"),i=n.attr("height"),s=n.index(),o,u,a=n.attr("data-alignment"),f=n.attr("data-portrait");if(typeof a==="undefined"||a===false||a===""){a=t.alignment}if(typeof f==="undefined"||f===false||f===""){f=t.portrait}if(f==false||f=="false"){if(r/i<w/E){var l=w/r;var c=Math.abs(E-i*l)*.5;switch(a){case"topLeft":o=0;break;case"topCenter":o=0;break;case"topRight":o=0;break;case"centerLeft":o="-"+c+"px";break;case"center":o="-"+c+"px";break;case"centerRight":o="-"+c+"px";break;case"bottomLeft":o="-"+c*2+"px";break;case"bottomCenter":o="-"+c*2+"px";break;case"bottomRight":o="-"+c*2+"px";break}if(t.fullpage==true){n.css({height:i*l,"margin-left":0,"margin-right":0,"margin-top":o,position:"absolute",visibility:"visible",left:0,width:w})}else{n.css({height:i*l,"margin-left":0,"margin-right":0,"margin-top":o,position:"absolute",visibility:"visible",width:w})}}else{var l=E/i;var c=Math.abs(w-r*l)*.5;switch(a){case"topLeft":u=0;break;case"topCenter":u="-"+c+"px";break;case"topRight":u="-"+c*2+"px";break;case"centerLeft":u=0;break;case"center":u="-"+c+"px";break;case"centerRight":u="-"+c*2+"px";break;case"bottomLeft":u=0;break;case"bottomCenter":u="-"+c+"px";break;case"bottomRight":u="-"+c*2+"px";break}n.css({height:E,"margin-left":u,"margin-right":u,"margin-top":0,position:"absolute",visibility:"visible",width:r*l})}}else{if(r/i<w/E){var l=E/i;var c=Math.abs(w-r*l)*.5;switch(a){case"topLeft":u=0;break;case"topCenter":u=c+"px";break;case"topRight":u=c*2+"px";break;case"centerLeft":u=0;break;case"center":u=c+"px";break;case"centerRight":u=c*2+"px";break;case"bottomLeft":u=0;break;case"bottomCenter":u=c+"px";break;case"bottomRight":u=c*2+"px";break}n.css({height:E,"margin-left":u,"margin-right":u,"margin-top":0,position:"absolute",visibility:"visible",width:r*l})}else{var l=w/r;var c=Math.abs(E-i*l)*.5;switch(a){case"topLeft":o=0;break;case"topCenter":o=0;break;case"topRight":o=0;break;case"centerLeft":o=c+"px";break;case"center":o=c+"px";break;case"centerRight":o=c+"px";break;case"bottomLeft":o=c*2+"px";break;case"bottomCenter":o=c*2+"px";break;case"bottomRight":o=c*2+"px";break}n.css({height:i*l,"margin-left":0,"margin-right":0,"margin-top":o,position:"absolute",visibility:"visible",width:w})}}})}var n;if(q==true){clearTimeout(n);n=setTimeout(r,200)}else{r()}q=true}function it(e){for(var t,n,r=e.length;r;t=parseInt(Math.random()*r),n=e[--r],e[r]=e[t],e[t]=n);return e}function st(e){return Math.ceil(e)==Math.floor(e)}function pt(){if(e(b).length){var n;if(!e(y).length){e(b).append("<div />");e(b).before('<div class="camera_prevThumbs hideNav"><div></div></div>').before('<div class="camera_nextThumbs hideNav"><div></div></div>');e("> div",b).append("<ul />");e("ul",b).width(_.length*(parseInt(t.thumbwidth)+2));e("ul",b).height(parseInt(t.thumbheight));e.each(_,function(t,n){if(e("> div",l).eq(t).attr("data-thumb")!=""){var r=e("> div",l).eq(t).attr("data-thumb"),i=new Image;i.src=r;e("ul",b).append('<li class="pix_thumb pix_thumb_'+t+'" />');e("li.pix_thumb_"+t,b).append(e(i).attr("class","camera_thumb"))}})}else{e.each(_,function(t,n){if(e("> div",l).eq(t).attr("data-thumb")!=""){var r=e("> div",l).eq(t).attr("data-thumb"),i=new Image;i.src=r;e("li.pag_nav_"+t,y).append(e(i).attr("class","camera_thumb").css({position:"absolute"}).animate({opacity:0},0));e("li.pag_nav_"+t+" > img",y).after('<div class="thumb_arrow" />');e("li.pag_nav_"+t+" > .thumb_arrow",y).animate({opacity:0},0)}});o.css({marginBottom:e(y).outerHeight()})}}else if(!e(b).length&&e(y).length){o.css({marginBottom:e(y).outerHeight()})}}function vt(){if(e(b).length&&!e(y).length){var t=e(b).outerWidth(),n=e("ul > li",b).outerWidth(),r=e("li.cameracurrent",b).length?e("li.cameracurrent",b).position():"",i=e("ul > li",b).length*e("ul > li",b).outerWidth(),s=e("ul",b).offset().left,u=e("> div",b).offset().left,a;if(s<0){a="-"+(u-s)}else{a=u-s}if(dt==true){e("ul",b).width(e("ul > li",b).length*e("ul > li",b).outerWidth());if(e(b).length&&!e(y).lenght){o.css({marginBottom:e(b).outerHeight()})}I();if(e(b).length&&!e(y).lenght){o.css({marginBottom:e(b).outerHeight()})}}dt=false;var f=e("li.cameracurrent",b).length?r.left:"",l=e("li.cameracurrent",b).length?r.left+e("li.cameracurrent",b).outerWidth():"";if(f<e("li.cameracurrent",b).outerWidth()){f=0}if(l-a>t){if(f+t<i){e("ul",b).animate({"margin-left":"-"+f+"px"},500,I)}else{e("ul",b).animate({"margin-left":"-"+(e("ul",b).outerWidth()-t)+"px"},500,I)}}else if(f-a<0){e("ul",b).animate({"margin-left":"-"+f+"px"},500,I)}else{e("ul",b).css({"margin-left":"auto","margin-right":"auto"});setTimeout(I,100)}}}function mt(){ft=0;var n=e(".camera_bar_cont",nt).width(),r=e(".camera_bar_cont",nt).height();if(a!="pie"){switch(tt){case"leftToRight":e("#"+f).css({right:n});break;case"rightToLeft":e("#"+f).css({left:n});break;case"topToBottom":e("#"+f).css({bottom:r});break;case"bottomToTop":e("#"+f).css({top:r});break}}else{ct.clearRect(0,0,t.pieDiameter,t.pieDiameter)}}function gt(n){l.addClass("camerasliding");Y=false;var r=parseFloat(e("div.cameraSlide.cameracurrent",c).index());if(n>0){var i=n-1}else if(r==D-1){var i=0}else{var i=r+1}var h=e(".cameraSlide:eq("+i+")",c);var p=e(".cameraSlide:eq("+(i+1)+")",c).addClass("cameranext");if(r!=i+1){p.hide()}e(".cameraContent",u).fadeOut(600);e(".camera_caption",u).show();e(".camerarelative",h).append(e("> div ",l).eq(i).find("> div.camera_effected"));e(".camera_target_content .cameraContent:eq("+i+")",o).append(e("> div ",l).eq(i).find("> div"));if(!e(".imgLoaded",h).length){var d=T[i];var v=new Image;v.src=d+"?"+(new Date).getTime();h.css("visibility","hidden");h.prepend(e(v).attr("class","imgLoaded").css("visibility","hidden"));var m,g;if(!e(v).get(0).complete||m=="0"||g=="0"||typeof m==="undefined"||m===false||typeof g==="undefined"||g===false){e(".camera_loader",o).delay(500).fadeIn(400);v.onload=function(){m=v.naturalWidth;g=v.naturalHeight;e(v).attr("data-alignment",M[i]).attr("data-portrait",O[i]);e(v).attr("width",m);e(v).attr("height",g);c.find(".cameraSlide_"+i).css("visibility","visible");W();gt(i+1);if(e(".camera_loader",o).is(":visible")){e(".camera_loader",o).fadeOut(400)}else{e(".camera_loader",o).css({visibility:"hidden"});e(".camera_loader",o).fadeOut(400,function(){e(".camera_loader",o).css({visibility:"visible"})})}}}}else{if(T.length>i+1&&!e(".imgLoaded",p).length){var S=T[i+1];var x=new Image;x.src=S+"?"+(new Date).getTime();p.prepend(e(x).attr("class","imgLoaded").css("visibility","hidden"));x.onload=function(){m=x.naturalWidth;g=x.naturalHeight;e(x).attr("data-alignment",M[i+1]).attr("data-portrait",O[i+1]);e(x).attr("width",m);e(x).attr("height",g);W()}}t.onLoaded.call(this);var N=t.rows,C=t.cols,k=1,L=0,A,_,P,H,j,F=new Array("simpleFade","curtainTopLeft","curtainTopRight","curtainBottomLeft","curtainBottomRight","curtainSliceLeft","curtainSliceRight","blindCurtainTopLeft","blindCurtainTopRight","blindCurtainBottomLeft","blindCurtainBottomRight","blindCurtainSliceBottom","blindCurtainSliceTop","stampede","mosaic","mosaicReverse","mosaicRandom","mosaicSpiral","mosaicSpiralReverse","topLeftBottomRight","bottomRightTopLeft","bottomLeftTopRight","topRightBottomLeft","scrollLeft","scrollRight","scrollTop","scrollBottom","scrollHorz");marginLeft=0,marginTop=0,opacityOnGrid=0;if(t.opacityOnGrid==true){opacityOnGrid=0}else{opacityOnGrid=1}var I=e(" > div",l).eq(i).attr("data-fx");if(s()&&t.mobileFx!=""&&t.mobileFx!="default"){H=t.mobileFx}else{if(typeof I!=="undefined"&&I!==false&&I!=="default"){H=I}else{H=t.fx}}if(H=="random"){H=it(F);H=H[0]}else{H=H;if(H.indexOf(",")>0){H=H.replace(/ /g,"");H=H.split(",");H=it(H);H=H[0]}}dataEasing=e(" > div",l).eq(i).attr("data-easing");mobileEasing=e(" > div",l).eq(i).attr("data-mobileEasing");if(s()&&t.mobileEasing!=""&&t.mobileEasing!="default"){if(typeof mobileEasing!=="undefined"&&mobileEasing!==false&&mobileEasing!=="default"){j=mobileEasing}else{j=t.mobileEasing}}else{if(typeof dataEasing!=="undefined"&&dataEasing!==false&&dataEasing!=="default"){j=dataEasing}else{j=t.easing}}A=e(" > div",l).eq(i).attr("data-slideOn");if(typeof A!=="undefined"&&A!==false){q=A}else{if(t.slideOn=="random"){var q=new Array("next","prev");q=it(q);q=q[0]}else{q=t.slideOn}}var R=e(" > div",l).eq(i).attr("data-time");if(typeof R!=="undefined"&&R!==false&&R!==""){_=parseFloat(R)}else{_=t.time}var U=e(" > div",l).eq(i).attr("data-transPeriod");if(typeof U!=="undefined"&&U!==false&&U!==""){P=parseFloat(U)}else{P=t.transPeriod}if(!e(l).hasClass("camerastarted")){H="simpleFade";q="next";j="";P=400;e(l).addClass("camerastarted")}switch(H){case"simpleFade":C=1;N=1;break;case"curtainTopLeft":if(t.slicedCols==0){C=t.cols}else{C=t.slicedCols}N=1;break;case"curtainTopRight":if(t.slicedCols==0){C=t.cols}else{C=t.slicedCols}N=1;break;case"curtainBottomLeft":if(t.slicedCols==0){C=t.cols}else{C=t.slicedCols}N=1;break;case"curtainBottomRight":if(t.slicedCols==0){C=t.cols}else{C=t.slicedCols}N=1;break;case"curtainSliceLeft":if(t.slicedCols==0){C=t.cols}else{C=t.slicedCols}N=1;break;case"curtainSliceRight":if(t.slicedCols==0){C=t.cols}else{C=t.slicedCols}N=1;break;case"blindCurtainTopLeft":if(t.slicedRows==0){N=t.rows}else{N=t.slicedRows}C=1;break;case"blindCurtainTopRight":if(t.slicedRows==0){N=t.rows}else{N=t.slicedRows}C=1;break;case"blindCurtainBottomLeft":if(t.slicedRows==0){N=t.rows}else{N=t.slicedRows}C=1;break;case"blindCurtainBottomRight":if(t.slicedRows==0){N=t.rows}else{N=t.slicedRows}C=1;break;case"blindCurtainSliceTop":if(t.slicedRows==0){N=t.rows}else{N=t.slicedRows}C=1;break;case"blindCurtainSliceBottom":if(t.slicedRows==0){N=t.rows}else{N=t.slicedRows}C=1;break;case"stampede":L="-"+P;break;case"mosaic":L=t.gridDifference;break;case"mosaicReverse":L=t.gridDifference;break;case"mosaicRandom":break;case"mosaicSpiral":L=t.gridDifference;k=1.7;break;case"mosaicSpiralReverse":L=t.gridDifference;k=1.7;break;case"topLeftBottomRight":L=t.gridDifference;k=6;break;case"bottomRightTopLeft":L=t.gridDifference;k=6;break;case"bottomLeftTopRight":L=t.gridDifference;k=6;break;case"topRightBottomLeft":L=t.gridDifference;k=6;break;case"scrollLeft":C=1;N=1;break;case"scrollRight":C=1;N=1;break;case"scrollTop":C=1;N=1;break;case"scrollBottom":C=1;N=1;break;case"scrollHorz":C=1;N=1;break}var z=0;var J=N*C;var K=w-Math.floor(w/C)*C;var Q=E-Math.floor(E/N)*N;var G;var et;var st=0;var ot=0;var ut=new Array;var at=new Array;var ht=new Array;while(z<J){ut.push(z);at.push(z);B.append('<div class="cameraappended" style="display:none; overflow:hidden; position:absolute; z-index:1000" />');var pt=e(".cameraappended:eq("+z+")",c);if(H=="scrollLeft"||H=="scrollRight"||H=="scrollTop"||H=="scrollBottom"||H=="scrollHorz"){Z.eq(i).clone().show().appendTo(pt)}else{if(q=="next"){Z.eq(i).clone().show().appendTo(pt)}else{Z.eq(r).clone().show().appendTo(pt)}}if(z%C<K){G=1}else{G=0}if(z%C==0){st=0}if(Math.floor(z/C)<Q){et=1}else{et=0}pt.css({height:Math.floor(E/N+et+1),left:st,top:ot,width:Math.floor(w/C+G+1)});e("> .cameraSlide",pt).css({height:E,"margin-left":"-"+st+"px","margin-top":"-"+ot+"px",width:w});st=st+pt.width()-1;if(z%C==C-1){ot=ot+pt.height()-1}z++}switch(H){case"curtainTopLeft":break;case"curtainBottomLeft":break;case"curtainSliceLeft":break;case"curtainTopRight":ut=ut.reverse();break;case"curtainBottomRight":ut=ut.reverse();break;case"curtainSliceRight":ut=ut.reverse();break;case"blindCurtainTopLeft":break;case"blindCurtainBottomLeft":ut=ut.reverse();break;case"blindCurtainSliceTop":break;case"blindCurtainTopRight":break;case"blindCurtainBottomRight":ut=ut.reverse();break;case"blindCurtainSliceBottom":ut=ut.reverse();break;case"stampede":ut=it(ut);break;case"mosaic":break;case"mosaicReverse":ut=ut.reverse();break;case"mosaicRandom":ut=it(ut);break;case"mosaicSpiral":var dt=N/2,yt,bt,wt,Et=0;for(wt=0;wt<dt;wt++){bt=wt;for(yt=wt;yt<C-wt-1;yt++){ht[Et++]=bt*C+yt}yt=C-wt-1;for(bt=wt;bt<N-wt-1;bt++){ht[Et++]=bt*C+yt}bt=N-wt-1;for(yt=C-wt-1;yt>wt;yt--){ht[Et++]=bt*C+yt}yt=wt;for(bt=N-wt-1;bt>wt;bt--){ht[Et++]=bt*C+yt}}ut=ht;break;case"mosaicSpiralReverse":var dt=N/2,yt,bt,wt,Et=J-1;for(wt=0;wt<dt;wt++){bt=wt;for(yt=wt;yt<C-wt-1;yt++){ht[Et--]=bt*C+yt}yt=C-wt-1;for(bt=wt;bt<N-wt-1;bt++){ht[Et--]=bt*C+yt}bt=N-wt-1;for(yt=C-wt-1;yt>wt;yt--){ht[Et--]=bt*C+yt}yt=wt;for(bt=N-wt-1;bt>wt;bt--){ht[Et--]=bt*C+yt}}ut=ht;break;case"topLeftBottomRight":for(var bt=0;bt<N;bt++)for(var yt=0;yt<C;yt++){ht.push(yt+bt)}at=ht;break;case"bottomRightTopLeft":for(var bt=0;bt<N;bt++)for(var yt=0;yt<C;yt++){ht.push(yt+bt)}at=ht.reverse();break;case"bottomLeftTopRight":for(var bt=N;bt>0;bt--)for(var yt=0;yt<C;yt++){ht.push(yt+bt)}at=ht;break;case"topRightBottomLeft":for(var bt=0;bt<N;bt++)for(var yt=C;yt>0;yt--){ht.push(yt+bt)}at=ht;break}e.each(ut,function(n,s){function d(){e(this).addClass("cameraeased");if(e(".cameraeased",c).length>=0){e(b).css({visibility:"visible"})}if(e(".cameraeased",c).length==J){vt();e(".moveFromLeft, .moveFromRight, .moveFromTop, .moveFromBottom, .fadeIn, .fadeFromLeft, .fadeFromRight, .fadeFromTop, .fadeFromBottom",u).each(function(){e(this).css("visibility","hidden")});Z.eq(i).show().css("z-index","999").removeClass("cameranext").addClass("cameracurrent");Z.eq(r).css("z-index","1").removeClass("cameracurrent");e(".cameraContent",u).eq(i).addClass("cameracurrent");if(r>=0){e(".cameraContent",u).eq(r).removeClass("cameracurrent")}t.onEndTransition.call(this);if(e("> div",l).eq(i).attr("data-video")!="hide"&&e(".cameraContent.cameracurrent .imgFake",u).length){e(".cameraContent.cameracurrent .imgFake",u).click()}var n=Z.eq(i).find(".fadeIn").length;var s=e(".cameraContent",u).eq(i).find(".moveFromLeft, .moveFromRight, .moveFromTop, .moveFromBottom, .fadeIn, .fadeFromLeft, .fadeFromRight, .fadeFromTop, .fadeFromBottom").length;if(n!=0){e(".cameraSlide.cameracurrent .fadeIn",u).each(function(){if(e(this).attr("data-easing")!=""){var t=e(this).attr("data-easing")}else{var t=j}var r=e(this);if(typeof r.attr("data-outerWidth")==="undefined"||r.attr("data-outerWidth")===false||r.attr("data-outerWidth")===""){var i=r.outerWidth();r.attr("data-outerWidth",i)}else{var i=r.attr("data-outerWidth")}if(typeof r.attr("data-outerHeight")==="undefined"||r.attr("data-outerHeight")===false||r.attr("data-outerHeight")===""){var s=r.outerHeight();r.attr("data-outerHeight",s)}else{var s=r.attr("data-outerHeight")}var o=r.position();var u=o.left;var a=o.top;var f=r.attr("class");var l=r.index();var c=r.parents(".camerarelative").outerHeight();var h=r.parents(".camerarelative").outerWidth();if(f.indexOf("fadeIn")!=-1){r.animate({opacity:0},0).css("visibility","visible").delay(_/n*.1*(l-1)).animate({opacity:1},_/n*.15,t)}else{r.css("visibility","visible")}})}e(".cameraContent.cameracurrent",u).show();if(s!=0){e(".cameraContent.cameracurrent .moveFromLeft, .cameraContent.cameracurrent .moveFromRight, .cameraContent.cameracurrent .moveFromTop, .cameraContent.cameracurrent .moveFromBottom, .cameraContent.cameracurrent .fadeIn, .cameraContent.cameracurrent .fadeFromLeft, .cameraContent.cameracurrent .fadeFromRight, .cameraContent.cameracurrent .fadeFromTop, .cameraContent.cameracurrent .fadeFromBottom",u).each(function(){if(e(this).attr("data-easing")!=""){var t=e(this).attr("data-easing")}else{var t=j}var n=e(this);var r=n.position();var i=r.left;var o=r.top;var u=n.attr("class");var a=n.index();var f=n.outerHeight();if(u.indexOf("moveFromLeft")!=-1){n.css({left:"-"+w+"px",right:"auto"});n.css("visibility","visible").delay(_/s*.1*(a-1)).animate({left:r.left},_/s*.15,t)}else if(u.indexOf("moveFromRight")!=-1){n.css({left:w+"px",right:"auto"});n.css("visibility","visible").delay(_/s*.1*(a-1)).animate({left:r.left},_/s*.15,t)}else if(u.indexOf("moveFromTop")!=-1){n.css({top:"-"+E+"px",bottom:"auto"});n.css("visibility","visible").delay(_/s*.1*(a-1)).animate({top:r.top},_/s*.15,t,function(){n.css({top:"auto",bottom:0})})}else if(u.indexOf("moveFromBottom")!=-1){n.css({top:E+"px",bottom:"auto"});n.css("visibility","visible").delay(_/s*.1*(a-1)).animate({top:r.top},_/s*.15,t)}else if(u.indexOf("fadeFromLeft")!=-1){n.animate({opacity:0},0).css({left:"-"+w+"px",right:"auto"});n.css("visibility","visible").delay(_/s*.1*(a-1)).animate({left:r.left,opacity:1},_/s*.15,t)}else if(u.indexOf("fadeFromRight")!=-1){n.animate({opacity:0},0).css({left:w+"px",right:"auto"});n.css("visibility","visible").delay(_/s*.1*(a-1)).animate({left:r.left,opacity:1},_/s*.15,t)}else if(u.indexOf("fadeFromTop")!=-1){n.animate({opacity:0},0).css({top:"-"+E+"px",bottom:"auto"});n.css("visibility","visible").delay(_/s*.1*(a-1)).animate({top:r.top,opacity:1},_/s*.15,t,function(){n.css({top:"auto",bottom:0})})}else if(u.indexOf("fadeFromBottom")!=-1){n.animate({opacity:0},0).css({bottom:"-"+f+"px"});n.css("visibility","visible").delay(_/s*.1*(a-1)).animate({bottom:"0",opacity:1},_/s*.15,t)}else if(u.indexOf("fadeIn")!=-1){n.animate({opacity:0},0).css("visibility","visible").delay(_/s*.1*(a-1)).animate({opacity:1},_/s*.15,t)}else{n.css("visibility","visible")}})}e(".cameraappended",c).remove();l.removeClass("camerasliding");Z.eq(r).hide();var o=e(".camera_bar_cont",nt).width(),h=e(".camera_bar_cont",nt).height(),d;if(a!="pie"){d=.05}else{d=.005}e("#"+f).animate({opacity:t.loaderOpacity},200);X=setInterval(function(){if(l.hasClass("stopped")){clearInterval(X)}if(a!="pie"){if(ft<=1.002&&!l.hasClass("stopped")&&!l.hasClass("paused")&&!l.hasClass("hovered")){ft=ft+d}else if(ft<=1&&(l.hasClass("stopped")||l.hasClass("paused")||l.hasClass("stopped")||l.hasClass("hovered"))){ft=ft}else{if(!l.hasClass("stopped")&&!l.hasClass("paused")&&!l.hasClass("hovered")){clearInterval(X);rt();e("#"+f).animate({opacity:0},200,function(){clearTimeout(V);V=setTimeout(mt,p);gt();t.onStartLoading.call(this)})}}switch(tt){case"leftToRight":e("#"+f).animate({right:o-o*ft},_*d,"linear");break;case"rightToLeft":e("#"+f).animate({left:o-o*ft},_*d,"linear");break;case"topToBottom":e("#"+f).animate({bottom:h-h*ft},_*d,"linear");break;case"bottomToTop":e("#"+f).animate({bottom:h-h*ft},_*d,"linear");break}}else{lt=ft;ct.clearRect(0,0,t.pieDiameter,t.pieDiameter);ct.globalCompositeOperation="destination-over";ct.beginPath();ct.arc(t.pieDiameter/2,t.pieDiameter/2,t.pieDiameter/2-t.loaderStroke,0,Math.PI*2,false);ct.lineWidth=t.loaderStroke;ct.strokeStyle=t.loaderBgColor;ct.stroke();ct.closePath();ct.globalCompositeOperation="source-over";ct.beginPath();ct.arc(t.pieDiameter/2,t.pieDiameter/2,t.pieDiameter/2-t.loaderStroke,0,Math.PI*2*lt,false);ct.lineWidth=t.loaderStroke-t.loaderPadding*2;ct.strokeStyle=t.loaderColor;ct.stroke();ct.closePath();if(ft<=1.002&&!l.hasClass("stopped")&&!l.hasClass("paused")&&!l.hasClass("hovered")){ft=ft+d}else if(ft<=1&&(l.hasClass("stopped")||l.hasClass("paused")||l.hasClass("hovered"))){ft=ft}else{if(!l.hasClass("stopped")&&!l.hasClass("paused")&&!l.hasClass("hovered")){clearInterval(X);rt();e("#"+f+", .camera_canvas_wrap",nt).animate({opacity:0},200,function(){clearTimeout(V);V=setTimeout(mt,p);gt();t.onStartLoading.call(this)})}}}},_*d)}}if(s%C<K){G=1}else{G=0}if(s%C==0){st=0}if(Math.floor(s/C)<Q){et=1}else{et=0}switch(H){case"simpleFade":height=E;width=w;opacityOnGrid=0;break;case"curtainTopLeft":height=0,width=Math.floor(w/C+G+1),marginTop="-"+Math.floor(E/N+et+1)+"px";break;case"curtainTopRight":height=0,width=Math.floor(w/C+G+1),marginTop="-"+Math.floor(E/N+et+1)+"px";break;case"curtainBottomLeft":height=0,width=Math.floor(w/C+G+1),marginTop=Math.floor(E/N+et+1)+"px";break;case"curtainBottomRight":height=0,width=Math.floor(w/C+G+1),marginTop=Math.floor(E/N+et+1)+"px";break;case"curtainSliceLeft":height=0,width=Math.floor(w/C+G+1);if(s%2==0){marginTop=Math.floor(E/N+et+1)+"px"}else{marginTop="-"+Math.floor(E/N+et+1)+"px"}break;case"curtainSliceRight":height=0,width=Math.floor(w/C+G+1);if(s%2==0){marginTop=Math.floor(E/N+et+1)+"px"}else{marginTop="-"+Math.floor(E/N+et+1)+"px"}break;case"blindCurtainTopLeft":height=Math.floor(E/N+et+1),width=0,marginLeft="-"+Math.floor(w/C+G+1)+"px";break;case"blindCurtainTopRight":height=Math.floor(E/N+et+1),width=0,marginLeft=Math.floor(w/C+G+1)+"px";break;case"blindCurtainBottomLeft":height=Math.floor(E/N+et+1),width=0,marginLeft="-"+Math.floor(w/C+G+1)+"px";break;case"blindCurtainBottomRight":height=Math.floor(E/N+et+1),width=0,marginLeft=Math.floor(w/C+G+1)+"px";break;case"blindCurtainSliceBottom":height=Math.floor(E/N+et+1),width=0;if(s%2==0){marginLeft="-"+Math.floor(w/C+G+1)+"px"}else{marginLeft=Math.floor(w/C+G+1)+"px"}break;case"blindCurtainSliceTop":height=Math.floor(E/N+et+1),width=0;if(s%2==0){marginLeft="-"+Math.floor(w/C+G+1)+"px"}else{marginLeft=Math.floor(w/C+G+1)+"px"}break;case"stampede":height=0;width=0;marginLeft=w*.2*(n%C-(C-Math.floor(C/2)))+"px";marginTop=E*.2*(Math.floor(n/C)+1-(N-Math.floor(N/2)))+"px";break;case"mosaic":height=0;width=0;break;case"mosaicReverse":height=0;width=0;marginLeft=Math.floor(w/C+G+1)+"px";marginTop=Math.floor(E/N+et+1)+"px";break;case"mosaicRandom":height=0;width=0;marginLeft=Math.floor(w/C+G+1)*.5+"px";marginTop=Math.floor(E/N+et+1)*.5+"px";break;case"mosaicSpiral":height=0;width=0;marginLeft=Math.floor(w/C+G+1)*.5+"px";marginTop=Math.floor(E/N+et+1)*.5+"px";break;case"mosaicSpiralReverse":height=0;width=0;marginLeft=Math.floor(w/C+G+1)*.5+"px";marginTop=Math.floor(E/N+et+1)*.5+"px";break;case"topLeftBottomRight":height=0;width=0;break;case"bottomRightTopLeft":height=0;width=0;marginLeft=Math.floor(w/C+G+1)+"px";marginTop=Math.floor(E/N+et+1)+"px";break;case"bottomLeftTopRight":height=0;width=0;marginLeft=0;marginTop=Math.floor(E/N+et+1)+"px";break;case"topRightBottomLeft":height=0;width=0;marginLeft=Math.floor(w/C+G+1)+"px";marginTop=0;break;case"scrollRight":height=E;width=w;marginLeft=-w;break;case"scrollLeft":height=E;width=w;marginLeft=w;break;case"scrollTop":height=E;width=w;marginTop=E;break;case"scrollBottom":height=E;width=w;marginTop=-E;break;case"scrollHorz":height=E;width=w;if(r==0&&i==D-1){marginLeft=-w}else if(r<i||r==D-1&&i==0){marginLeft=w}else{marginLeft=-w}break}var h=e(".cameraappended:eq("+s+")",c);if(typeof X!=="undefined"){clearInterval(X);clearTimeout(V);V=setTimeout(mt,P+L)}if(e(y).length){e(".camera_pag li",o).removeClass("cameracurrent");e(".camera_pag li",o).eq(i).addClass("cameracurrent")}if(e(b).length){e("li",b).removeClass("cameracurrent");e("li",b).eq(i).addClass("cameracurrent");e("li",b).not(".cameracurrent").find("img").animate({opacity:.5},0);e("li.cameracurrent img",b).animate({opacity:1},0);e("li",b).hover(function(){e("img",this).stop(true,false).animate({opacity:1},150)},function(){if(!e(this).hasClass("cameracurrent")){e("img",this).stop(true,false).animate({opacity:.5},150)}})}var p=parseFloat(P)+parseFloat(L);if(H=="scrollLeft"||H=="scrollRight"||H=="scrollTop"||H=="scrollBottom"||H=="scrollHorz"){t.onStartTransition.call(this);p=0;h.delay((P+L)/J*at[n]*k*.5).css({display:"block",height:height,"margin-left":marginLeft,"margin-top":marginTop,width:width}).animate({height:Math.floor(E/N+et+1),"margin-top":0,"margin-left":0,width:Math.floor(w/C+G+1)},P-L,j,d);Z.eq(r).delay((P+L)/J*at[n]*k*.5).animate({"margin-left":marginLeft*-1,"margin-top":marginTop*-1},P-L,j,function(){e(this).css({"margin-top":0,"margin-left":0})})}else{t.onStartTransition.call(this);p=parseFloat(P)+parseFloat(L);if(q=="next"){h.delay((P+L)/J*at[n]*k*.5).css({display:"block",height:height,"margin-left":marginLeft,"margin-top":marginTop,width:width,opacity:opacityOnGrid}).animate({height:Math.floor(E/N+et+1),"margin-top":0,"margin-left":0,opacity:1,width:Math.floor(w/C+G+1)},P-L,j,d)}else{Z.eq(i).show().css("z-index","999").addClass("cameracurrent");Z.eq(r).css("z-index","1").removeClass("cameracurrent");e(".cameraContent",u).eq(i).addClass("cameracurrent");e(".cameraContent",u).eq(r).removeClass("cameracurrent");h.delay((P+L)/J*at[n]*k*.5).css({display:"block",height:Math.floor(E/N+et+1),"margin-top":0,"margin-left":0,opacity:1,width:Math.floor(w/C+G+1)}).animate({height:height,"margin-left":marginLeft,"margin-top":marginTop,width:width,opacity:opacityOnGrid},P-L,j,d)}}})}}var r={alignment:"center",autoAdvance:true,mobileAutoAdvance:true,barDirection:"leftToRight",barPosition:"bottom",cols:6,easing:"easeInOutExpo",mobileEasing:"",fx:"random",mobileFx:"",gridDifference:250,height:"50%",imagePath:"images/",hover:true,loader:"pie",loaderColor:"#eeeeee",loaderBgColor:"#222222",loaderOpacity:.8,loaderPadding:2,loaderStroke:7,minHeight:"200px",navigation:true,navigationHover:true,mobileNavHover:true,opacityOnGrid:false,overlayer:true,pagination:true,playPause:false,pauseOnClick:true,pieDiameter:38,piePosition:"rightTop",portrait:false,rows:4,slicedCols:12,slicedRows:8,slideOn:"random",thumbnails:false,thumbheight:"100",thumbwidth:"75",time:7e3,transPeriod:1500,fullpage:false,lightbox:"none",mobileimageresolution:0,onEndTransition:function(){},onLoaded:function(){},onStartLoading:function(){},onStartTransition:function(){}};var t=e.extend({},r,t);var o=e(this).addClass("camera_wrap");if(t.fullpage==true){e(document.body).css("background","none").prepend(o);o.css({height:"100%","margin-left":0,"margin-right":0,"margin-top":0,position:"fixed",visibility:"visible",left:0,right:0,"min-width":"100%","min-height":"100%",width:"100%","z-index":"-1"})}o.wrapInner('<div class="camera_src" />').wrapInner('<div class="camera_fakehover" />');var u=e(".camera_fakehover",o);u.append('<div class="camera_target"></div>');if(t.overlayer==true){u.append('<div class="camera_overlayer"></div>')}u.append('<div class="camera_target_content"></div>');var a;if(navigator.userAgent.match(/MSIE 8.0/i)||navigator.userAgent.match(/MSIE 7.0/i)||navigator.userAgent.match(/MSIE 6.0/i)){a="bar"}else{a=t.loader}if(a=="pie"){u.append('<div class="camera_pie"></div>')}else if(a=="bar"){u.append('<div class="camera_bar"></div>')}else{u.append('<div class="camera_bar" style="display:none"></div>')}if(t.playPause==true){u.append('<div class="camera_commands"></div>')}if(t.navigation==true){u.append('<div class="camera_prev"><span></span></div>').append('<div class="camera_next"><span></span></div>')}if(t.thumbnails==true){o.append('<div class="camera_thumbs_cont" />')}if(t.thumbnails==true&&t.pagination!=true){e(".camera_thumbs_cont",o).wrap("<div />").wrap('<div class="camera_thumbs" />').wrap("<div />").wrap('<div class="camera_command_wrap" />')}if(t.pagination==true){o.append('<div class="camera_pag"></div>')}o.append('<div class="camera_loader"></div>');e(".camera_caption",o).each(function(){e(this).wrapInner("<div />")});var f="pie_"+o.attr("id"),l=e(".camera_src",o),c=e(".camera_target",o),h=e(".camera_target_content",o),p=e(".camera_pie",o),d=e(".camera_bar",o),v=e(".camera_prev",o),m=e(".camera_next",o),g=e(".camera_commands",o),y=e(".camera_pag",o),b=e(".camera_thumbs_cont",o);var w,E;var S=parseInt(e(document.body).width());imgresolution=0;if(t.mobileimageresolution){var x=t.mobileimageresolution.split(",");for(i=0;i<x.length;i++){if(S<=x[i]&&(imgresolution!=0&&x[i]<=imgresolution||imgresolution==0&&S<Math.max.apply(Math,x))){imgresolution=x[i]}}}var T=new Array;var N;e("> div",l).each(function(){N=e(this).attr("data-src");if(imgresolution){imgsrctmp=N.split("/");imgnametmp=imgsrctmp[imgsrctmp.length-1];imgsrctmp[imgsrctmp.length-1]=imgresolution+"/"+imgnametmp;N=imgsrctmp.join("/")}T.push(N)});var C=new Array;var k=new Array;var L=new Array;e("> div",l).each(function(){if(e(this).attr("data-link")){C.push(e(this).attr("data-link"))}else{C.push("")}if(e(this).attr("data-rel")){k.push('rel="'+e(this).attr("data-rel")+'" ')}else{k.push("")}if(e(this).attr("data-title")){L.push('title="'+e(this).attr("data-title")+'" ')}else{L.push("")}});var A=new Array;e("> div",l).each(function(){if(e(this).attr("data-target")){A.push(e(this).attr("data-target"))}else{A.push("")}});var O=new Array;e("> div",l).each(function(){if(e(this).attr("data-portrait")){O.push(e(this).attr("data-portrait"))}else{O.push("")}});var M=new Array;e("> div",l).each(function(){if(e(this).attr("data-alignment")){M.push(e(this).attr("data-alignment"))}else{M.push("")}});var _=new Array;e("> div",l).each(function(){if(e(this).attr("data-thumb")){_.push(e(this).attr("data-thumb"))}else{_.push("")}});var D=T.length;e(h).append('<div class="cameraContents" />');var P;for(P=0;P<D;P++){e(".cameraContents",h).append('<div class="cameraContent" />');if(C[P]!=""){var H=e("> div ",l).eq(P).attr("data-box");if(typeof H!=="undefined"&&H!==false&&H!=""){H='data-box="'+e("> div ",l).eq(P).attr("data-box")+'"'}else{H=""}e(".camera_target_content .cameraContent:eq("+P+")",o).append('<a class="camera_link" '+k[P]+L[P]+'href="'+C[P]+'" '+H+' target="'+A[P]+'"></a>')}}e(".camera_caption",o).each(function(){var t=e(this).parent().index(),n=o.find(".cameraContent").eq(t);e(this).appendTo(n)});c.append('<div class="cameraCont" />');var B=e(".cameraCont",o);var j;for(j=0;j<D;j++){B.append('<div class="cameraSlide cameraSlide_'+j+'" />');var F=e("> div:eq("+j+")",l);c.find(".cameraSlide_"+j).clone(F)}e(window).bind("load resize pageshow",function(){vt();I()});B.append('<div class="cameraSlide cameraSlide_'+j+'" />');var q;o.show();var w=c.width();var E=c.height();var R;e(window).bind("resize pageshow",function(){if(q==true){W()}e("ul",b).animate({"margin-top":0},0,vt);if(!l.hasClass("paused")){l.addClass("paused");if(e(".camera_stop",nt).length){e(".camera_stop",nt).hide();e(".camera_play",nt).show();if(a!="none"){e("#"+f).hide()}}else{if(a!="none"){e("#"+f).hide()}}clearTimeout(R);R=setTimeout(function(){l.removeClass("paused");if(e(".camera_play",nt).length){e(".camera_play",nt).hide();e(".camera_stop",nt).show();if(a!="none"){e("#"+f).fadeIn()}}else{if(a!="none"){e("#"+f).fadeIn()}}},1500)}});z();var X,V;var J,K,Q,g,y;var G,Y;if(s()&&t.mobileAutoAdvance!=""){K=t.mobileAutoAdvance}else{K=t.autoAdvance}if(K==false){l.addClass("paused")}if(s()&&t.mobileNavHover!=""){Q=t.mobileNavHover}else{Q=t.navigationHover}if(l.length!=0){var Z=e(".cameraSlide",c);Z.wrapInner('<div class="camerarelative" />');var et;var tt=t.barDirection;var nt=o;e("iframe",u).each(function(){var t=e(this);var n=t.attr("src");t.attr("data-src",n);var r=t.parent().index("#"+o.attr("id")+" .camera_src > div");e(".camera_target_content .cameraContent:eq("+r+")",o).append(t)});function rt(){e("iframe",u).each(function(){e(".camera_caption",u).show();var n=e(this);var r=n.attr("data-src");n.attr("src",r);var i=t.imagePath+"blank.gif";var s=new Image;s.src=i;if(t.height.indexOf("%")!=-1){var a=Math.round(w/(100/parseFloat(t.height)));if(t.minHeight!=""&&a<parseFloat(t.minHeight)){E=parseFloat(t.minHeight)}else{E=a}}else if(t.height=="auto"){E=o.height()}else{E=parseFloat(t.height)}n.after(e(s).attr({"class":"imgFake",width:w,height:E}));var f=n.clone();n.remove();e(s).bind("click",function(){if(e(this).css("position")=="absolute"){e(this).remove();if(r.indexOf("vimeo")!=-1||r.indexOf("youtube")!=-1){if(r.indexOf("?")!=-1){autoplay="&autoplay=1"}else{autoplay="?autoplay=1"}}else if(r.indexOf("dailymotion")!=-1){if(r.indexOf("?")!=-1){autoplay="&autoPlay=1"}else{autoplay="?autoPlay=1"}}f.attr("src",r+autoplay);Y=true}else{e(this).css({position:"absolute",top:0,left:0,zIndex:10}).after(f);f.css({position:"absolute",top:0,left:0,zIndex:9})}})})}rt();if(t.hover==true){if(!s()){u.hover(function(){l.addClass("hovered")},function(){l.removeClass("hovered")})}}if(Q==true){e(v,o).animate({opacity:0},0);e(m,o).animate({opacity:0},0);e(g,o).animate({opacity:0},0);if(s()){u.on("vmouseover",function(){e(v,o).animate({opacity:1},200);e(m,o).animate({opacity:1},200);e(g,o).animate({opacity:1},200)});u.on("vmouseout",function(){e(v,o).delay(500).animate({opacity:0},200);e(m,o).delay(500).animate({opacity:0},200);e(g,o).delay(500).animate({opacity:0},200)})}else{u.hover(function(){e(v,o).animate({opacity:1},200);e(m,o).animate({opacity:1},200);e(g,o).animate({opacity:1},200)},function(){e(v,o).animate({opacity:0},200);e(m,o).animate({opacity:0},200);e(g,o).animate({opacity:0},200)})}}nt.on("click",".camera_stop",function(){K=false;l.addClass("paused");if(e(".camera_stop",nt).length){e(".camera_stop",nt).hide();e(".camera_play",nt).show();if(a!="none"){e("#"+f).hide()}}else{if(a!="none"){e("#"+f).hide()}}});nt.on("click",".camera_play",function(){K=true;l.removeClass("paused");if(e(".camera_play",nt).length){e(".camera_play",nt).hide();e(".camera_stop",nt).show();if(a!="none"){e("#"+f).show()}}else{if(a!="none"){e("#"+f).show()}}});if(t.pauseOnClick==true){e(".camera_target_content",u).mouseup(function(){K=false;l.addClass("paused");e(".camera_stop",nt).hide();e(".camera_play",nt).show();e("#"+f).hide()})}e(".cameraContent, .imgFake",u).hover(function(){G=true},function(){G=false});e(".cameraContent, .imgFake",u).bind("click",function(){if(Y==true&&G==true){K=false;e(".camera_caption",u).hide();l.addClass("paused");e(".camera_stop",nt).hide();e(".camera_play",nt).show();e("#"+f).hide()}})}if(a!="pie"){d.append('<span class="camera_bar_cont" />');e(".camera_bar_cont",d).animate({opacity:t.loaderOpacity},0).css({position:"absolute",left:0,right:0,top:0,bottom:0,"background-color":t.loaderBgColor}).append('<span id="'+f+'" />');e("#"+f).animate({opacity:0},0);var ot=e("#"+f);ot.css({position:"absolute","background-color":t.loaderColor});switch(t.barPosition){case"left":d.css({right:"auto",width:t.loaderStroke});break;case"right":d.css({left:"auto",width:t.loaderStroke});break;case"top":d.css({bottom:"auto",height:t.loaderStroke});break;case"bottom":d.css({top:"auto",height:t.loaderStroke});break}switch(tt){case"leftToRight":ot.css({left:0,right:0,top:t.loaderPadding,bottom:t.loaderPadding});break;case"rightToLeft":ot.css({left:0,right:0,top:t.loaderPadding,bottom:t.loaderPadding});break;case"topToBottom":ot.css({left:t.loaderPadding,right:t.loaderPadding,top:0,bottom:0});break;case"bottomToTop":ot.css({left:t.loaderPadding,right:t.loaderPadding,top:0,bottom:0});break}}else{p.append('<canvas id="'+f+'"></canvas>');var ut;var ot=document.getElementById(f);ot.setAttribute("width",t.pieDiameter);ot.setAttribute("height",t.pieDiameter);var at;switch(t.piePosition){case"leftTop":at="left:0; top:0;";break;case"rightTop":at="right:0; top:0;";break;case"leftBottom":at="left:0; bottom:0;";break;case"rightBottom":at="right:0; bottom:0;";break}ot.setAttribute("style","position:absolute; z-index:1002; "+at);var ft;var lt;if(ot&&ot.getContext){var ct=ot.getContext("2d");ct.rotate(Math.PI*(3/2));ct.translate(-t.pieDiameter,0)}}if(a=="none"||K==false){e("#"+f).hide();e(".camera_canvas_wrap",nt).hide()}if(e(y).length){e(y).append('<ul class="camera_pag_ul" />');var ht;for(ht=0;ht<D;ht++){e(".camera_pag_ul",o).append('<li class="pag_nav_'+ht+'" style="position:relative; z-index:1002"><span><span>'+ht+"</span></span></li>")}e(".camera_pag_ul li",o).hover(function(){e(this).addClass("camera_hover");if(e(".camera_thumb",this).length){var t=e(".camera_thumb",this).outerWidth(),n=e(".camera_thumb",this).outerHeight(),r=e(this).outerWidth();e(".camera_thumb",this).show().css({top:"-"+n+"px",left:"-"+(t-r)/2+"px"}).animate({opacity:1,"margin-top":"-3px"},200);e(".thumb_arrow",this).show().animate({opacity:1,"margin-top":"-3px"},200)}},function(){e(this).removeClass("camera_hover");e(".camera_thumb",this).animate({"margin-top":"-20px",opacity:0},200,function(){e(this).css({marginTop:"5px"}).hide()});e(".thumb_arrow",this).animate({"margin-top":"-20px",opacity:0},200,function(){e(this).css({marginTop:"5px"}).hide()})})}t.onStartLoading.call(this);gt();pt();var dt=true;if(e(g).length){e(g).append('<div class="camera_play"></div>').append('<div class="camera_stop"></div>');if(K==true){e(".camera_play",nt).hide();e(".camera_stop",nt).show()}else{e(".camera_stop",nt).hide();e(".camera_play",nt).show()}}mt();e(".moveFromLeft, .moveFromRight, .moveFromTop, .moveFromBottom, .fadeIn, .fadeFromLeft, .fadeFromRight, .fadeFromTop, .fadeFromBottom",u).each(function(){e(this).css("visibility","hidden")});U();if(e(v).length){e(v).click(function(){if(!l.hasClass("camerasliding")){var n=parseFloat(e(".cameraSlide.cameracurrent",c).index());clearInterval(X);rt();e("#"+f+", .camera_canvas_wrap",o).animate({opacity:0},0);mt();if(n!=0){gt(n)}else{gt(D)}t.onStartLoading.call(this)}})}if(e(m).length){e(m).click(function(){if(!l.hasClass("camerasliding")){var n=parseFloat(e(".cameraSlide.cameracurrent",c).index());clearInterval(X);rt();e("#"+f+", .camera_canvas_wrap",nt).animate({opacity:0},0);mt();if(n==D-1){gt(1)}else{gt(n+2)}t.onStartLoading.call(this)}})}if(s()){u.bind("swipeleft",function(n){if(!l.hasClass("camerasliding")){var r=parseFloat(e(".cameraSlide.cameracurrent",c).index());clearInterval(X);rt();e("#"+f+", .camera_canvas_wrap",nt).animate({opacity:0},0);mt();if(r==D-1){gt(1)}else{gt(r+2)}t.onStartLoading.call(this)}});u.bind("swiperight",function(n){if(!l.hasClass("camerasliding")){var r=parseFloat(e(".cameraSlide.cameracurrent",c).index());clearInterval(X);rt();e("#"+f+", .camera_canvas_wrap",nt).animate({opacity:0},0);mt();if(r!=0){gt(r)}else{gt(D)}t.onStartLoading.call(this)}})}if(e(y).length){e(".camera_pag li",o).click(function(){if(!l.hasClass("camerasliding")){var n=parseFloat(e(this).index());var r=parseFloat(e(".cameraSlide.cameracurrent",c).index());if(n!=r){clearInterval(X);rt();e("#"+f+", .camera_canvas_wrap",nt).animate({opacity:0},0);mt();gt(n+1);t.onStartLoading.call(this)}}})}if(e(b).length){e(".pix_thumb img",b).click(function(){if(!l.hasClass("camerasliding")){var n=parseFloat(e(this).parents("li").index());var r=parseFloat(e(".cameracurrent",c).index());if(n!=r){clearInterval(X);rt();e("#"+f+", .camera_canvas_wrap",nt).animate({opacity:0},0);e(".pix_thumb",b).removeClass("cameracurrent");e(this).parents("li").addClass("cameracurrent");mt();gt(n+1);vt();t.onStartLoading.call(this)}}});e(".camera_thumbs_cont .camera_prevThumbs",nt).hover(function(){e(this).stop(true,false).animate({opacity:1},250)},function(){e(this).stop(true,false).animate({opacity:.7},250)});e(".camera_prevThumbs",nt).click(function(){var t=0,n=e(b).outerWidth(),r=e("ul",b).offset().left,i=e("> div",b).offset().left,s=i-r;e(".camera_visThumb",b).each(function(){var n=e(this).outerWidth();t=t+n});if(s-t>0){e("ul",b).animate({"margin-left":"-"+(s-t)+"px"},500,I)}else{e("ul",b).animate({"margin-left":0},500,I)}});e(".camera_thumbs_cont .camera_nextThumbs",nt).hover(function(){e(this).stop(true,false).animate({opacity:1},250)},function(){e(this).stop(true,false).animate({opacity:.7},250)});e(".camera_nextThumbs",nt).click(function(){var t=0,n=e(b).outerWidth(),r=e("ul",b).outerWidth(),i=e("ul",b).offset().left,s=e("> div",b).offset().left,o=s-i;e(".camera_visThumb",b).each(function(){var n=e(this).outerWidth();t=t+n});if(o+t+t<r){e("ul",b).animate({"margin-left":"-"+(o+t)+"px"},500,I)}else{e("ul",b).animate({"margin-left":"-"+(r-n)+"px"},500,I)}})}}})(jQuery);(function(e){e.fn.cameraStop=function(){var t=e(this),n=e(".camera_src",t),r="pie_"+t.index();n.addClass("stopped");if(e(".camera_showcommands").length){var i=e(".camera_thumbs_wrap",t)}else{var i=t}}})(jQuery);(function(e){e.fn.cameraPause=function(){var t=e(this);var n=e(".camera_src",t);n.addClass("paused")}})(jQuery);(function(e){e.fn.cameraResume=function(){var t=e(this);var n=e(".camera_src",t);if(typeof autoAdv==="undefined"||autoAdv!==true){n.removeClass("paused")}}})(jQuery);

//	jQuery Mobile framework customized for Camera slideshow, made by
//	'jquery.mobile.define.js',
//	'jquery.ui.widget.js',
//	'jquery.mobile.widget.js',
//	'jquery.mobile.media.js',
//	'jquery.mobile.support.js',
//	'jquery.mobile.vmouse.js',
//	'jquery.mobile.event.js',
//	'jquery.mobile.core.js'

window.define=function(){Array.prototype.slice.call(arguments).pop()(window.jQuery)};define(["jquery"],function(a){(function(a,b){if(a.cleanData){var c=a.cleanData;a.cleanData=function(b){for(var d=0,e;(e=b[d])!=null;d++){a(e).triggerHandler("remove")}c(b)}}else{var d=a.fn.remove;a.fn.remove=function(b,c){return this.each(function(){if(!c){if(!b||a.filter(b,[this]).length){a("*",this).add([this]).each(function(){a(this).triggerHandler("remove")})}}return d.call(a(this),b,c)})}}a.widget=function(b,c,d){var e=b.split(".")[0],f;b=b.split(".")[1];f=e+"-"+b;if(!d){d=c;c=a.Widget}a.expr[":"][f]=function(c){return!!a.data(c,b)};a[e]=a[e]||{};a[e][b]=function(a,b){if(arguments.length){this._createWidget(a,b)}};var g=new c;g.options=a.extend(true,{},g.options);a[e][b].prototype=a.extend(true,g,{namespace:e,widgetName:b,widgetEventPrefix:a[e][b].prototype.widgetEventPrefix||b,widgetBaseClass:f},d);a.widget.bridge(b,a[e][b])};a.widget.bridge=function(c,d){a.fn[c]=function(e){var f=typeof e==="string",g=Array.prototype.slice.call(arguments,1),h=this;e=!f&&g.length?a.extend.apply(null,[true,e].concat(g)):e;if(f&&e.charAt(0)==="_"){return h}if(f){this.each(function(){var d=a.data(this,c);if(!d){throw"cannot call methods on "+c+" prior to initialization; "+"attempted to call method '"+e+"'"}if(!a.isFunction(d[e])){throw"no such method '"+e+"' for "+c+" widget instance"}var f=d[e].apply(d,g);if(f!==d&&f!==b){h=f;return false}})}else{this.each(function(){var b=a.data(this,c);if(b){b.option(e||{})._init()}else{a.data(this,c,new d(e,this))}})}return h}};a.Widget=function(a,b){if(arguments.length){this._createWidget(a,b)}};a.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",options:{disabled:false},_createWidget:function(b,c){a.data(c,this.widgetName,this);this.element=a(c);this.options=a.extend(true,{},this.options,this._getCreateOptions(),b);var d=this;this.element.bind("remove."+this.widgetName,function(){d.destroy()});this._create();this._trigger("create");this._init()},_getCreateOptions:function(){var b={};if(a.metadata){b=a.metadata.get(element)[this.widgetName]}return b},_create:function(){},_init:function(){},destroy:function(){this.element.unbind("."+this.widgetName).removeData(this.widgetName);this.widget().unbind("."+this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass+"-disabled "+"ui-state-disabled")},widget:function(){return this.element},option:function(c,d){var e=c;if(arguments.length===0){return a.extend({},this.options)}if(typeof c==="string"){if(d===b){return this.options[c]}e={};e[c]=d}this._setOptions(e);return this},_setOptions:function(b){var c=this;a.each(b,function(a,b){c._setOption(a,b)});return this},_setOption:function(a,b){this.options[a]=b;if(a==="disabled"){this.widget()[b?"addClass":"removeClass"](this.widgetBaseClass+"-disabled"+" "+"ui-state-disabled").attr("aria-disabled",b)}return this},enable:function(){return this._setOption("disabled",false)},disable:function(){return this._setOption("disabled",true)},_trigger:function(b,c,d){var e=this.options[b];c=a.Event(c);c.type=(b===this.widgetEventPrefix?b:this.widgetEventPrefix+b).toLowerCase();d=d||{};if(c.originalEvent){for(var f=a.event.props.length,g;f;){g=a.event.props[--f];c[g]=c.originalEvent[g]}}this.element.trigger(c,d);return!(a.isFunction(e)&&e.call(this.element[0],c,d)===false||c.isDefaultPrevented())}}})(jQuery)});define(["jquery","./jquery.ui.widget"],function(a){(function(a,b){a.widget("mobile.widget",{_createWidget:function(){a.Widget.prototype._createWidget.apply(this,arguments);this._trigger("init")},_getCreateOptions:function(){var c=this.element,d={};a.each(this.options,function(a){var e=c.jqmData(a.replace(/[A-Z]/g,function(a){return"-"+a.toLowerCase()}));if(e!==b){d[a]=e}});return d},enhanceWithin:function(b){var c=a.mobile.closestPageData(a(b)),d=c&&c.keepNativeSelector()||"";a(this.options.initSelector,b).not(d)[this.widgetName]()}})})(jQuery)});define(["jquery","./jquery.mobile.core"],function(a){(function(a,b){var c=a(window),d=a("html");a.mobile.media=function(){var b={},c=a("<div id='jquery-mediatest'>"),e=a("<body>").append(c);return function(a){if(!(a in b)){var f=document.createElement("style"),g="@media "+a+" { #jquery-mediatest { position:absolute; } }";f.type="text/css";if(f.styleSheet){f.styleSheet.cssText=g}else{f.appendChild(document.createTextNode(g))}d.prepend(e).prepend(f);b[a]=c.css("position")==="absolute";e.add(f).remove()}return b[a]}}()})(jQuery)});define(["jquery","./jquery.mobile.media"],function(a){(function(a,b){function m(){var b=location.protocol+"//"+location.host+location.pathname+"ui-dir/",d=a("head base"),e=null,f="",g,h;if(!d.length){d=e=a("<base>",{href:b}).appendTo("head")}else{f=d.attr("href")}g=a("<a href='testurl' />").prependTo(c);h=g[0].href;d[0].href=f||location.pathname;if(e){e.remove()}return h.indexOf(b)===0}function l(){var b="transform-3d";return k("perspective","10px","moz")||a.mobile.media("(-"+e.join("-"+b+"),(-")+"-"+b+"),("+b+")")}function k(a,b,c){var d=document.createElement("div"),f=function(a){return a.charAt(0).toUpperCase()+a.substr(1)},g=function(a){return"-"+a.charAt(0).toLowerCase()+a.substr(1)+"-"},h=function(c){var e=g(c)+a+": "+b+";",h=f(c),i=h+f(a);d.setAttribute("style",e);if(!!d.style[i]){k=true}},j=c?[c]:e,k;for(i=0;i<j.length;i++){h(j[i])}return!!k}function j(a){var c=a.charAt(0).toUpperCase()+a.substr(1),f=(a+" "+e.join(c+" ")+c).split(" ");for(var g in f){if(d[f[g]]!==b){return true}}}var c=a("<body>").prependTo("html"),d=c[0].style,e=["Webkit","Moz","O"],f="palmGetResource"in window,g=window.operamini&&{}.toString.call(window.operamini)==="[object OperaMini]",h=window.blackberry;a.extend(a.mobile,{browser:{}});a.mobile.browser.ie=function(){var a=3,b=document.createElement("div"),c=b.all||[];while(b.innerHTML="<!--[if gt IE "+ ++a+"]><br><![endif]-->",c[0]){}return a>4?a:!a}();a.extend(a.support,{orientation:"orientation"in window&&"onorientationchange"in window,touch:"ontouchend"in document,cssTransitions:"WebKitTransitionEvent"in window||k("transition","height 100ms linear"),pushState:"pushState"in history&&"replaceState"in history,mediaquery:a.mobile.media("only all"),cssPseudoElement:!!j("content"),touchOverflow:!!j("overflowScrolling"),cssTransform3d:l(),boxShadow:!!j("boxShadow")&&!h,scrollTop:("pageXOffset"in window||"scrollTop"in document.documentElement||"scrollTop"in c[0])&&!f&&!g,dynamicBaseTag:m()});c.remove();var n=function(){var a=window.navigator.userAgent;return a.indexOf("Nokia")>-1&&(a.indexOf("Symbian/3")>-1||a.indexOf("Series60/5")>-1)&&a.indexOf("AppleWebKit")>-1&&a.match(/(BrowserNG|NokiaBrowser)\/7\.[0-3]/)}();a.mobile.ajaxBlacklist=window.blackberry&&!window.WebKitPoint||g||n;if(n){a(function(){a("head link[rel='stylesheet']").attr("rel","alternate stylesheet").attr("rel","stylesheet")})}if(!a.support.boxShadow){a("html").addClass("ui-mobile-nosupport-boxshadow")}})(jQuery)});define(["jquery"],function(a){(function(a,b,c,d){function O(b){var c=b.substr(1);return{setup:function(d,f){if(!M(this)){a.data(this,e,{})}var g=a.data(this,e);g[b]=true;k[b]=(k[b]||0)+1;if(k[b]===1){t.bind(c,H)}a(this).bind(c,N);if(s){k["touchstart"]=(k["touchstart"]||0)+1;if(k["touchstart"]===1){t.bind("touchstart",I).bind("touchend",L).bind("touchmove",K).bind("scroll",J)}}},teardown:function(d,f){--k[b];if(!k[b]){t.unbind(c,H)}if(s){--k["touchstart"];if(!k["touchstart"]){t.unbind("touchstart",I).unbind("touchmove",K).unbind("touchend",L).unbind("scroll",J)}}var g=a(this),h=a.data(this,e);if(h){h[b]=false}g.unbind(c,N);if(!M(this)){g.removeData(e)}}}}function N(){}function M(b){var c=a.data(b,e),d;if(c){for(d in c){if(c[d]){return true}}}return false}function L(a){if(r){return}B();var b=y(a.target),c;G("vmouseup",a,b);if(!o){var d=G("vclick",a,b);if(d&&d.isDefaultPrevented()){c=w(a).changedTouches[0];p.push({touchID:v,x:c.clientX,y:c.clientY});q=true}}G("vmouseout",a,b);o=false;E()}function K(b){if(r){return}var c=w(b).touches[0],d=o,e=a.vmouse.moveDistanceThreshold;o=o||Math.abs(c.pageX-m)>e||Math.abs(c.pageY-n)>e,flags=y(b.target);if(o&&!d){G("vmousecancel",b,flags)}G("vmousemove",b,flags);E()}function J(a){if(r){return}if(!o){G("vmousecancel",a,y(a.target))}o=true;E()}function I(b){var c=w(b).touches,d,e;if(c&&c.length===1){d=b.target;e=y(d);if(e.hasVirtualBinding){v=u++;a.data(d,f,v);F();D();o=false;var g=w(b).touches[0];m=g.pageX;n=g.pageY;G("vmouseover",b,e);G("vmousedown",b,e)}}}function H(b){var c=a.data(b.target,f);if(!q&&(!v||v!==c)){var d=G("v"+b.type,b);if(d){if(d.isDefaultPrevented()){b.preventDefault()}if(d.isPropagationStopped()){b.stopPropagation()}if(d.isImmediatePropagationStopped()){b.stopImmediatePropagation()}}}}function G(b,c,d){var e;if(d&&d[b]||!d&&z(c.target,b)){e=x(c,b);a(c.target).trigger(e)}return e}function F(){if(l){clearTimeout(l);l=0}}function E(){F();l=setTimeout(function(){l=0;C()},a.vmouse.resetTimerDuration)}function D(){A()}function C(){v=0;p.length=0;q=false;B()}function B(){r=true}function A(){r=false}function z(b,c){var d;while(b){d=a.data(b,e);if(d&&(!c||d[c])){return b}b=b.parentNode}return null}function y(b){var c={},d,f;while(b){d=a.data(b,e);for(f in d){if(d[f]){c[f]=c.hasVirtualBinding=true}}b=b.parentNode}return c}function x(b,c){var e=b.type,f,g,i,k,l,m,n,o;b=a.Event(b);b.type=c;f=b.originalEvent;g=a.event.props;if(e.search(/mouse/)>-1){g=j}if(f){for(n=g.length,k;n;){k=g[--n];b[k]=f[k]}}if(e.search(/mouse(down|up)|click/)>-1&&!b.which){b.which=1}if(e.search(/^touch/)!==-1){i=w(f);e=i.touches;l=i.changedTouches;m=e&&e.length?e[0]:l&&l.length?l[0]:d;if(m){for(o=0,len=h.length;o<len;o++){k=h[o];b[k]=m[k]}}}return b}function w(a){while(a&&typeof a.originalEvent!=="undefined"){a=a.originalEvent}return a}var e="virtualMouseBindings",f="virtualTouchID",g="vmouseover vmousedown vmousemove vmouseup vclick vmouseout vmousecancel".split(" "),h="clientX clientY pageX pageY screenX screenY".split(" "),i=a.event.mouseHooks?a.event.mouseHooks.props:[],j=a.event.props.concat(i),k={},l=0,m=0,n=0,o=false,p=[],q=false,r=false,s="addEventListener"in c,t=a(c),u=1,v=0;a.vmouse={moveDistanceThreshold:10,clickDistanceThreshold:10,resetTimerDuration:1500};for(var P=0;P<g.length;P++){a.event.special[g[P]]=O(g[P])}if(s){c.addEventListener("click",function(b){var c=p.length,d=b.target,e,g,h,i,j,k;if(c){e=b.clientX;g=b.clientY;threshold=a.vmouse.clickDistanceThreshold;h=d;while(h){for(i=0;i<c;i++){j=p[i];k=0;if(h===d&&Math.abs(j.x-e)<threshold&&Math.abs(j.y-g)<threshold||a.data(h,f)===j.touchID){b.preventDefault();b.stopPropagation();return}}h=h.parentNode}}},true)}})(jQuery,window,document)});define(["jquery","./jquery.mobile.core","./jquery.mobile.media","./jquery.mobile.support","./jquery.mobile.vmouse"],function(a){(function(a,b,c){function i(b,c,d){var e=d.type;d.type=c;a.event.handle.call(b,d);d.type=e}a.each(("touchstart touchmove touchend orientationchange throttledresize "+"tap taphold swipe swipeleft swiperight scrollstart scrollstop").split(" "),function(b,c){a.fn[c]=function(a){return a?this.bind(c,a):this.trigger(c)};a.attrFn[c]=true});var d=a.support.touch,e="touchmove scroll",f=d?"touchstart":"mousedown",g=d?"touchend":"mouseup",h=d?"touchmove":"mousemove";a.event.special.scrollstart={enabled:true,setup:function(){function g(a,c){d=c;i(b,d?"scrollstart":"scrollstop",a)}var b=this,c=a(b),d,f;c.bind(e,function(b){if(!a.event.special.scrollstart.enabled){return}if(!d){g(b,true)}clearTimeout(f);f=setTimeout(function(){g(b,false)},50)})}};a.event.special.tap={setup:function(){var b=this,c=a(b);c.bind("vmousedown",function(d){function k(a){j();if(e==a.target){i(b,"tap",a)}}function j(){h();c.unbind("vclick",k).unbind("vmouseup",h);a(document).unbind("vmousecancel",j)}function h(){clearTimeout(g)}if(d.which&&d.which!==1){return false}var e=d.target,f=d.originalEvent,g;c.bind("vmouseup",h).bind("vclick",k);a(document).bind("vmousecancel",j);g=setTimeout(function(){i(b,"taphold",a.Event("taphold"))},750)})}};a.event.special.swipe={scrollSupressionThreshold:10,durationThreshold:1e3,horizontalDistanceThreshold:30,verticalDistanceThreshold:75,setup:function(){var b=this,d=a(b);d.bind(f,function(b){function j(b){if(!f){return}var c=b.originalEvent.touches?b.originalEvent.touches[0]:b;i={time:(new Date).getTime(),coords:[c.pageX,c.pageY]};if(Math.abs(f.coords[0]-i.coords[0])>a.event.special.swipe.scrollSupressionThreshold){b.preventDefault()}}var e=b.originalEvent.touches?b.originalEvent.touches[0]:b,f={time:(new Date).getTime(),coords:[e.pageX,e.pageY],origin:a(b.target)},i;d.bind(h,j).one(g,function(b){d.unbind(h,j);if(f&&i){if(i.time-f.time<a.event.special.swipe.durationThreshold&&Math.abs(f.coords[0]-i.coords[0])>a.event.special.swipe.horizontalDistanceThreshold&&Math.abs(f.coords[1]-i.coords[1])<a.event.special.swipe.verticalDistanceThreshold){f.origin.trigger("swipe").trigger(f.coords[0]>i.coords[0]?"swipeleft":"swiperight")}}f=i=c})})}};(function(a,b){function j(){var a=e();if(a!==f){f=a;c.trigger("orientationchange")}}var c=a(b),d,e,f,g,h,i={0:true,180:true};if(a.support.orientation){g=a.mobile.media("all and (orientation: landscape)");h=i[b.orientation];if(g&&h||!g&&!h){i={"-90":true,90:true}}}a.event.special.orientationchange=d={setup:function(){if(a.support.orientation&&a.mobile.orientationChangeEnabled){return false}f=e();c.bind("throttledresize",j)},teardown:function(){if(a.support.orientation&&a.mobile.orientationChangeEnabled){return false}c.unbind("throttledresize",j)},add:function(a){var b=a.handler;a.handler=function(a){a.orientation=e();return b.apply(this,arguments)}}};a.event.special.orientationchange.orientation=e=function(){var c=true,d=document.documentElement;if(a.support.orientation){c=i[b.orientation]}else{c=d&&d.clientWidth/d.clientHeight<1.1}return c?"portrait":"landscape"}})(jQuery,b);(function(){a.event.special.throttledresize={setup:function(){a(this).bind("resize",c)},teardown:function(){a(this).unbind("resize",c)}};var b=250,c=function(){f=(new Date).getTime();g=f-d;if(g>=b){d=f;a(this).trigger("throttledresize")}else{if(e){clearTimeout(e)}e=setTimeout(c,b-g)}},d=0,e,f,g})();a.each({scrollstop:"scrollstart",taphold:"tap",swipeleft:"swipe",swiperight:"swipe"},function(b,c){a.event.special[b]={setup:function(){a(this).bind(c,a.noop)}}})})(jQuery,this)});define(["jquery","../external/requirejs/text!../version.txt","./jquery.mobile.widget"],function(a,b){(function(a,c,d){var e={};a.mobile=a.extend({},{version:b,ns:"",subPageUrlKey:"ui-page",activePageClass:"ui-page-active",activeBtnClass:"ui-btn-active",focusClass:"ui-focus",ajaxEnabled:true,hashListeningEnabled:true,linkBindingEnabled:true,defaultPageTransition:"fade",maxTransitionWidth:false,minScrollBack:10,touchOverflowEnabled:false,defaultDialogTransition:"pop",loadingMessage:"loading",pageLoadErrorMessage:"Error Loading Page",loadingMessageTextVisible:false,loadingMessageTheme:"a",pageLoadErrorMessageTheme:"e",autoInitializePage:true,pushStateEnabled:true,orientationChangeEnabled:true,gradeA:function(){return a.support.mediaquery||a.mobile.browser.ie&&a.mobile.browser.ie>=7},keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91},silentScroll:function(b){if(a.type(b)!=="number"){b=a.mobile.defaultHomeScroll}a.event.special.scrollstart.enabled=false;setTimeout(function(){c.scrollTo(0,b);a(document).trigger("silentscroll",{x:0,y:b})},20);setTimeout(function(){a.event.special.scrollstart.enabled=true},150)},nsNormalizeDict:e,nsNormalize:function(b){if(!b){return}return e[b]||(e[b]=a.camelCase(a.mobile.ns+b))},getInheritedTheme:function(a,b){var c=a[0],d="",e=/ui-(bar|body)-([a-z])\b/,f,g;while(c){var f=c.className||"";if((g=e.exec(f))&&(d=g[2])){break}c=c.parentNode}return d||b||"a"},closestPageData:function(a){return a.closest(':jqmData(role="page"), :jqmData(role="dialog")').data("page")}},a.mobile);a.fn.jqmData=function(b,c){var d;if(typeof b!="undefined"){d=this.data(b?a.mobile.nsNormalize(b):b,c)}return d};a.jqmData=function(b,c,d){var e;if(typeof c!="undefined"){e=a.data(b,c?a.mobile.nsNormalize(c):c,d)}return e};a.fn.jqmRemoveData=function(b){return this.removeData(a.mobile.nsNormalize(b))};a.jqmRemoveData=function(b,c){return a.removeData(b,a.mobile.nsNormalize(c))};a.fn.removeWithDependents=function(){a.removeWithDependents(this)};a.removeWithDependents=function(b){var c=a(b);(c.jqmData("dependents")||a()).remove();c.remove()};a.fn.addDependents=function(b){a.addDependents(a(this),b)};a.addDependents=function(b,c){var d=a(b).jqmData("dependents")||a();a(b).jqmData("dependents",a.merge(d,c))};a.fn.getEncodedText=function(){return a("<div/>").text(a(this).text()).html()};var f=a.find,g=/:jqmData\(([^)]*)\)/g;a.find=function(b,c,d,e){b=b.replace(g,"[data-"+(a.mobile.ns||"")+"$1]");return f.call(this,b,c,d,e)};a.extend(a.find,f);a.find.matches=function(b,c){return a.find(b,null,null,c)};a.find.matchesSelector=function(b,c){return a.find(c,null,null,[b]).length>0}})(jQuery,this)})





//Javacritpt Modulo mod_jse_megamenu

var HoverIntent = new Class({
	Implements:[Options, Events],
	options:{
			sensitivity: 7,
			interval: 100,
			timeout: 0,
			over:function(){},
			out:function(){}
	},
	initialize:function(element, options){
		this.setOptions(options);
		this.ob = element;
		var self = this;
		this.ob.addEvents({
			mouseenter:function(e){self.handleHover(e);},
			mouseleave:function(e){self.handleHover(e);}
		});
	},
	track:function(ev){
		this.cX = ev.page.x;
		this.cY = ev.page.y;
	},
	_compare:function(ev,ob){
		var self = this;
		ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
		// compare mouse positions to see if they've crossed the threshold
		if ( ( Math.abs(this.pX-this.cX) + Math.abs(this.pY-this.cY) ) < this.options.sensitivity ) {
			ob.removeEvent("mousemove",self.track);
			// set hoverIntent state to true (so mouseOut can be called)
			ob.hoverIntent_s = 1;
			return this.options.over.apply(ob,[ev]);
		} else {
			// set previous coordinates for next time
			this.pX = this.cX; this.pY = this.cY;
			// use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
			ob.hoverIntent_t = (function(){self._compare(ev, ob);}).delay(this.options.interval);
		}
	},
	_delay:function(ev, ob){
		ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
		ob.hoverIntent_s = 0;
//		console.log("delayed call");
		return this.options.out.apply(ob,[ev]);
	},
	handleHover:function(e){
		var ev = e;
		var self = this;
		// cancel hoverIntent timer if it exists
		if (this.ob.hoverIntent_t !== undefined) {
			this.ob.hoverIntent_t = clearTimeout(self.ob.hoverIntent_t);
		}
		// if e.type == "mouseenter"
		if (e.type == "mouseenter" || e.type == "mouseover") {
			// set "previous" X and Y position based on initial entry point
			this.pX = ev.page.x;
			this.pY = ev.page.y;
			// update "current" X and Y position based on mousemove
			this.ob.addEvent("mousemove",function(e){
				self.track(e);
			});
			// start polling interval (self-calling timeout) to compare mouse coordinates over time
			if (this.ob.hoverIntent_s != 1) {
				this.ob.hoverIntent_t = (function(){
					self._compare(ev,self.ob);
				}).delay(self.options.interval);
			}

		// else e.type == "mouseleave"
		} else if (e.type == "mouseleave" || e.type == "mouseout") {
			// unbind expensive mousemove event
			this.ob.removeEvent("mousemove",function(e){
				self.track(e);
			});
			// if hoverIntent state is true, then call the mouseOut function after the specified delay
			if (this.ob.hoverIntent_s == 1) {
				this.ob.hoverIntent_t = (function(){
					self._delay(ev, self.ob);
				}).delay(self.options.interval);
			}
		}
	}
});

Element.implement({
  hoverIntent : function(options) {
    new HoverIntent(this, options);
    return this;
  }
});


//Javacritpt Modulo mod_jse_megamenu


var JRMenu = window.JRMenu || {};
var jsMegaMenuMoo = new Class({

	initialize: function(menu, options){
        this.options = Object.append({
            slide:    true, //enable slide
            duration: 300, //slide speed. lower for slower, bigger for faster
            fading: false, //Enable fading
            bgopacity: 0.9, //set the transparent background. 0 to disable, 0<bgopacity<1: the opacity of the background
            delayHide: 500,
            menutype: 'horizontal',
            direction: 'down',
            action: 'mouseenter', //mouseenter or click
            hidestyle: 'normal',
            hover_delay: 50
        }, options || {});

        //ignore delayHide if no animation
        if (!this.options.slide && !this.options.fading) this.options.delayHide = 10;

        this.menu = $(menu);

        JRMenu.inst = this;
        this.menu.addClass('mm-enable');
        this.childopen = new Array();
        this.imgloaded = false;
        this.loaded = false;

        //if(this.options.menutype == 'horizontal'){
           this.addnavbtn(); 
        //}

        window.addEvent('load', this.start.bind(this));
        this.start();
    },
    
    addnavbtn: function(){
        if (!(Browser.ie && Browser.version < 9)) {
            var mainNav = this.menu.getElement('ul.megamenu.level0');

            if(mainNav){
                var megaMenu = this.menu;
                    
                var	toggleBtn = new Element ('div', {id:'js-megaMenuToggle', 'class': 'megaMenuToggle', html: 'Menu', styles: { display: 'none'}}).inject (megaMenu, 'before');
				new Element('span', {'class': 'megaMenuToggle-icon'}).inject(toggleBtn);
				
                toggleBtn.addEvent('click', function(e){
                    e.stop();
			
                    if(megaMenu.getStyle('display') == 'block'){
                        megaMenu.removeClass('active').setStyle('display', 'none');
                        toggleBtn.removeClass('active');
                    } else {
                        megaMenu.addClass('active').setStyle('display', 'block');
                        toggleBtn.addClass('active');
                    }
                });
                
                $(document).addEvent('click', function(){
                    if(!megaMenu.hasClass('mm-enable')){
                        megaMenu.removeClass('active').setStyle('display', 'none');
                    }
                    
                    toggleBtn.removeClass('active');
                });
            }

            JRMenu.mmenuid = null;
            window.addEvent('resize', function(){
                clearTimeout(JRMenu.mmenuid);
                JRMenu.mmenuid = setTimeout(function(){JRMenu.inst.start();}, 100);
            });
        }
    },
    
    detect: function(){
        var toggleBtn = $('js-megaMenuToggle'),
            rs = true;
        if(toggleBtn){
            rs = toggleBtn.getComputedStyle('display') == 'none';

            if(rs != this.menu.hasClass('mm-enable')){

                this.menu[rs ? 'addClass' : 'removeClass']('mm-enable');

                this.menu.setStyle('display', rs ? 'block' : 'none');
            }
        }
        
        return rs;
    },

    start: function () {
        //do nothing if loaded

        if (!this.detect() || !this.imgloaded || this.loaded) ;
        
        if (this.loaded) return;

        this.menu = $(this.menu);
        //preload images
        var images = this.menu.getElements ('img');
        if (images && images.length && !this.imageloaded) {
            var imgs = [];
            images.each (function (image) {imgs.push(image.src)});
        }

        //mark as called
        this.loaded = true;

        //get wrapper
        p = this.menu;
        while (p=p.getParent()) {
            if (p.hasClass ('main') || p.hasClass ('wrap')) {this.wrapper = p; break;}
        }
        this.items = this.menu.getElements ('li.mega');
        //this.items.setStyle ('position', 'relative');
        this.items.each (function(li) {
            //link item
            if ((a = li.getElement('a.mega')) && this.isChild (a, li)) li.a = a;
            else li.a = null;
            //parent
            li._parent = this.getParent (li);
            //child content
            if ((childcontent = li.getElement('.childcontent')) && this.isChild (childcontent, li)) {
                li.childcontent = childcontent;
                li.childcontent_inner = li.childcontent.getElement ('.childcontent-inner-wrap');
                var coor = li.childcontent_inner.getCoordinates ();
                li._w = li.getElement('.childcontent-inner').offsetWidth;
                li._h = li.getElement('.childcontent-inner').offsetHeight;

                li.level0 = li.getParent().hasClass('level0');
                //luanND change the childcontent to equal width with childcontent_inner
                //li.childcontent.setStyles ({'width':li._w+10, 'height':li._h});
				li.childcontent.setStyles ({'width':li._w, 'height':li._h});
                li.childcontent_inner.setStyles ({'width':li._w});
                //fix for overflow
                li.childcontent_inner1 = li.childcontent.getElement ('.childcontent-inner');
                li.childcontent_inner1.ol = false;
                if (li.childcontent_inner1.getStyle ('overflow') == 'auto' || li.childcontent_inner1.getStyle ('overflow') == 'scroll') {
                    li.childcontent_inner1.ol = true;
                    //fix for ie6/7
                    if (window.ie6 || window.ie7) {
                        li.childcontent_inner1.setStyle ('position', 'relative');
                    }

                    if (window.ie6) {
                        li.childcontent_inner1.setStyle ('height', li.childcontent_inner1.getStyle ('max-height') || 400);
                    }
                }

                //show direction
                if (this.options.direction == 'up') {
                    if (li.level0) {
                        li.childcontent.setStyle ('top', -li.childcontent.offsetHeight); //ajust top position
                    } else {
                        li.childcontent.setStyle ('bottom', 0);
                    }
                }
            }
            else li.childcontent = null;

            if (li.childcontent && this.options.bgopacity) {
                //Make transparent background
                var bg = new Element ('div', {'class':'childcontent-bg'});
                bg.injectTop (li.childcontent_inner);
                bg.setStyles ({'width':'100%', 'height':li._h, 'opacity':this.options.bgopacity,
                                'position': 'absolute', 'top': 0, 'left': 0, 'z-index': 1
                                });
                if (li.childcontent.getStyle('background')) bg.setStyle ('background', li.childcontent.getStyle('background'));
                if (li.childcontent.getStyle('background-image')) bg.setStyle ('background-image', li.childcontent.getStyle('background-image'));
                if (li.childcontent.getStyle('background-repeat')) bg.setStyle ('background-repeat', li.childcontent.getStyle('background-repeat'));
                if (li.childcontent.getStyle('background-color')) bg.setStyle ('background-color', li.childcontent.getStyle('background-color'));
                li.childcontent.setStyle ('background', 'none');
                li.childcontent_inner.setStyles ({'position':'relative', 'z-index': 2});
            }

            if (li.childcontent && (this.options.slide || this.options.fading)) {
                //li.childcontent.setStyles ({'width': li._w});

                li.childcontent.setStyles ({'left':'auto'});
                if (li.childcontent.hasClass ('right')) li.childcontent.setStyle ('right', 0);
                if (this.options.slide) {
                    li.childcontent.setStyles ({'left':'auto', 'overflow':'hidden'});
                    if (li.level0) {
                        if(this.options.menutype == 'horizontal') {
                            if (this.options.direction == 'up') {
                                li.childcontent_inner.setStyle ('bottom', -li._h-20);
                            } else {
                                li.childcontent_inner.setStyle ('margin-top', -li._h-20);
                            }
                        } 
                        else {
                            
                            if (this.options.direction == 'righttoleft') {
                                
                                li.childcontent_inner.setStyle ('margin-left', li._w-20);
                            } else {
                                li.childcontent_inner.setStyle ('margin-left', -li._w-20);
                            }
                        }

                    } else {
                        if (this.options.menutype == 'vertical' && this.options.direction == 'righttoleft'){
                            li.childcontent_inner.setStyle ('margin-left', li._w-20);
                        }
                        else{
                            li.childcontent_inner.setStyle ('margin-left', -li._w-20); 
                        }
                    }
                }
                if (this.options.fading) {
                    li.childcontent_inner.setStyle ('opacity', 0);
                }
                //Init Fx.Styles for childcontent
                //li.fx = new Fx.Styles(li.childcontent_inner, {duration: this.options.duration, transition: Fx.Transitions.linear, onComplete: this.itemAnimDone.bind(this, li)});
                //li.fx = new Fx.Tween (li.childcontent_inner, {duration: this.options.duration, transition: Fx.Transitions.linear, onComplete: this.itemAnimDone.bind(this, li)});
                // Dohq: Fix for use both fade & slide
                li.fx = new Fx.Morph (li.childcontent_inner, {duration: this.options.duration, transition: Fx.Transitions.linear, onComplete: this.itemAnimDone.bind(this, li)});
                //effect
                //li.eff_on = {p:[],to:[]};
                //li.eff_off = {p:[],to:[]};
                li.eff_on  = {};
                li.eff_off = {};
                if (this.options.slide) {
                    if (li.level0) {
                        if(this.options.menutype == 'horizontal') {
                            if (this.options.direction == 'up') {
                                li.eff_on ['bottom'] = 0;
                                li.eff_off ['bottom'] = -li._h;
                            } else { 
                                li.eff_on ['margin-top'] = 0;
                                li.eff_off ['margin-top'] = -li._h;
                            }
                        }
                        else {
                                if (this.options.direction == 'righttoleft'){
                                    li.eff_on['margin-left'] = 0;
                                    li.eff_off['margin-left'] = li._w;
                                }
                                else{
                                    li.eff_on['margin-left'] = 0;
                                    li.eff_off['margin-left'] = -li._w;
                                }
                        }
                    } else {
                          if (this.options.menutype == 'vertical' && this.options.direction == 'righttoleft'){
                            li.eff_on['margin-left'] = 0;
                            li.eff_off['margin-left'] = li._w;
                          }
                          else{
                            li.eff_on['margin-left'] = 0;
                            li.eff_off['margin-left'] = -li._w;  
                          }
                    }
                }
                if (this.options.fading) {
                    li.eff_on ['opacity'] = 1;
                    li.eff_off['opacity'] = 0;
                }
            }

            if (this.options.action=='click' && li.childcontent) {
                li.addEvent('click', function(e) {

                    var event = new DOMEvent (e);
                    if (li.hasClass ('group')) return;
                    if (li.childcontent) {
                        if (li.status == 'open') {
                            if (this.cursorIn (li, event)) {
                                this.itemHide (li);
                            } else {
                                this.itemHideOthers(li);
                            }
                        } else {
                            this.itemShow (li);
                        }
                    } else {
                        if (li.a) location.href = li.a.href;
                    }
                    event.stopPropagation();
                }.bind (this));
            }
            if(li.timer == undefined) li.timer = null;
            if (this.options.action == 'mouseover' || this.options.action == 'mouseenter') {
                var config = {
					over: function(e) {
						if (li.hasClass ('group')) return;

						//fn.delay(li.timer);
						//this.itemShow.delay(this.options.hover_delay, this, li);
						this.itemShow(li);

						if (!e.stopped) {
							// Comment this line because of error in IE 8. Version 3.0.9
//							e.stopPropagation();
							e.stopped = true; //make sure the stop function is call only once
						}
					}.bind (this),
					interval: this.options.hover_delay,
					out: function(e) {
						if (li.hasClass ('group')) return;
						//fn.delay(li.timer);
						if (li.childcontent) li.timer = this.itemHide.delay(this.options.delayHide, this, [li, e]);
						else this.itemHide (li, e);
						if (!e.stopped) {
							// Comment this line because of error in IE 8. Version 3.0.9
//							e.stopPropagation();
							e.stopped = true; //make sure the stop function is call only once
						}
					}.bind (this)
				};

                li.hoverIntent(config);

				//if has childcontent, don't goto link before open childcontent - fix for touch screen
                if (li.a && li.childcontent) {
                    li.clickable = false;
                    li.a.addEvent ('click',function (e){
                        if (!li.clickable) {
                            new DOMEvent(e).stop();
                        }
                    }.bind (this));
                }

				if(li.childcontent) {
					mobile_button = new Element ('span', {'class':'arrow-icon', html: '', styles: { display: 'none'}}).inject (li.childcontent, 'before');
					mobile_button.addEvent('click', function(e) {
						var event = new DOMEvent (e);
						//if (li.hasClass ('group')) return;
						
						if (li.childcontent) {
							
							if (li.status == 'open') {
								if (this.cursorIn (li, event)) {
									this.itemHide (li);
								} else {
									this.itemHideOthers(li);
								}
							} else {
								this.itemShow (li);
							}
						} else {
							
							if (li.a) location.href = li.a.href;
						}
						event.stopPropagation();
					}.bind (this));
				}

                //stop if click on menu item - prevent raise event to container => hide all open submenu
                li.addEvent ('click', function (e) {new DOMEvent(e).stopPropagation()});
            }

            //when click on a link - close all open childcontent
            if (li.a && !li.childcontent) {
                li.a.addEvent ('click',function (e){
                    this.itemHideOthers (null);
                    //Remove current class
                    this.menu.getElements ('.active').removeClass ('active');
                    //Add current class
                    var p = li;
                    while (p) {
                        p.addClass ('active');
                        p.a.addClass ('active');
                        p = p._parent;
                    }
                    new DOMEvent (e).stopPropagation();//prevent to raise event up
                }.bind (this));
            }

            if (li.childcontent) this.positionSubmenu (li);
        },this);

        //click on windows will close all submenus
        var container = $('wrapper');
        if (!container) container = document.body;
        if (container.addEvent) {
            container.addEvent('click',function (e) {
                this.itemHideOthers(null);
            }.bind (this));
        } else {
            container.attachEvent('click',function (e) {
                this.itemHideOthers(null);
            }.bind (this));
        }

        if (this.options.slide || this.options.fading) {
            //hide all content child
            this.menu.getElements('.childcontent').setStyle ('display', 'none');
        }

    },

    getParent: function (li) {
        var p = li;
        while ((p=p.getParent())) {
            if (this.items.contains (p) && !p.hasClass ('group')) return p;
            if (!p || p == this.menu) return null;
        }
    },

    cursorIn: function (el, event) {
        if (!el || !event) return false;
        var pos = Object.merge(el.getPosition(), {'w':el.offsetWidth, 'h': el.offsetHeight});;
        var cursor = {'x': event.page.x, 'y': event.page.y};

        if (cursor.x>pos.x && cursor.x<pos.x+el.offsetWidth
                && cursor.y>pos.y && cursor.y<pos.y+el.offsetHeight) return true;
        return false;
    },

    isChild: function (child, parent) {
        return !!parent.getChildren().contains (child);
    },

    itemOver: function (li) {
        if (li.hasClass ('haschild'))
            li.removeClass ('haschild').addClass ('haschild-over');
        li.addClass ('over');
        if (li.a) {
            li.a.addClass ('over');
        }
    },

    itemOut: function (li) {
        if (li.hasClass ('haschild-over'))
            li.removeClass ('haschild-over').addClass ('haschild');
        li.removeClass ('over');
        if (li.a) {
            li.a.removeClass ('over');
        }
    },

    itemShow: function (li) {
        clearTimeout(li.timer);
        if (li.status == 'open') return; //don't need do anything
        //Setup the class
        this.itemOver (li);
        //push to show queue
        li.status = 'open';
        this.enableclick.delay (100, this, li);
        this.childopen.push (li);
        //hide other
        this.itemHideOthers (li);
        if (li.childcontent) {
            //reposition the submenu
            this.positionSubmenu (li);
        }


        if (li.fx == null || li.childcontent == null) return;

        li.childcontent.setStyle ('display', 'block');

        li.childcontent.setStyles ({'overflow': 'hidden'});
        if (li.childcontent_inner1.ol) {
			li.childcontent_inner1.setStyles ({'overflow': 'hidden'});
		}
        li.fx.cancel();

        li.fx.start (li.eff_on);
        //li.fx.start (li.eff_on.p, li.eff_on.to);
        //if (li._parent) this.itemShow (li._parent);
    },

    itemHide: function (li, e) {
        if (e && e.page) { //if event
            if (this.cursorIn (li, e) || this.cursorIn (li.childcontent, e)) {
                return;
            } //cursor in li
            var p=li._parent;
            if (p && !this.cursorIn (p, e) && !this.cursorIn(p.childcontent, e)) {
                p.fireEvent ('mouseleave', e); //fire mouseleave event
            }
        }
        clearTimeout(li.timer);
        this.itemOut(li);
        li.status = 'close';
        this.childopen.erase (li);

        if (li.fx == null || li.childcontent == null) return;

        if (li.childcontent.getStyle ('opacity') == 0) return;
        li.childcontent.setStyles ({'overflow': 'hidden'});
        if (li.childcontent_inner1.ol) li.childcontent_inner1.setStyles ({'overflow': 'hidden'});
        li.fx.cancel();
        switch (this.options.hidestyle) {
        case 'fast':
            li.fx.options.duration = 100;
            li.fx.start(Object.merge(li.eff_off,{'opacity':0}));
            //li.fx.start ($merge(li.eff_off,{'opacity':0}));
            //li.fx.start(li.eff_off.p, li.eff_off.to);
            break;
        case 'fastwhenshow': //when other show
            if (!e) { //force hide, not because of event => hide fast
                li.fx.start (Object.merge(li.eff_off,{'opacity':0}));
                //li.fx.options.duration = 300;
                //li.fx.start ($merge(li.eff_off,{'opacity':0}));
                //li.fx.start(li.eff_off.p, li.eff_off.to);
            } else {    //hide as normal
                li.fx.start (li.eff_off);
                //li.fx.start (li.eff_off);
                //li.fx.start(li.eff_off.p, li.eff_off.to);
            }
            break;
        case 'normal':
        default:
            li.fx.start (li.eff_off);
            //li.fx.start(li.eff_off.p, li.eff_off.to);
            break;
        }
        //li.fx.start (li.eff_off);
    },

    itemAnimDone: function (li) {
        //hide done
        if (li.status == 'close'){
            //reset duration and enable opacity if not fading
            if (this.options.hidestyle.test (/fast/)) {
                li.fx.options.duration = this.options.duration;
                if (!this.options.fading) li.childcontent_inner.setStyle ('opacity', 1);
            }
            //hide
            li.childcontent.setStyles ({'display': 'none'});
            this.disableclick.delay (100, this, li);
        }

        //show done
        if (li.status == 'open'){
            li.childcontent.setStyles ({'overflow': ''});
            if (li.childcontent_inner1.ol) li.childcontent_inner1.setStyles ({'overflow-y': 'auto'});
            li.childcontent_inner.setStyle ('opacity', 1);
            li.childcontent.setStyles ({'display': 'block'});
        }
    },

    itemHideOthers: function (el) {
        var fakeevent = null
        if (el && !el.childcontent) fakeevent = {};
        var curopen = this.childopen;
        curopen.each (function(li) {
            if (li && typeof (li.status) != 'undefined' && (!el || (li != el && !(el !== li && li.contains(el))))) {
                this.itemHide(li, fakeevent);
            }
        },this);
    },

    enableclick: function (li) {
        if (li.a && li.childcontent) li.clickable = true;
    },
    disableclick: function (li) {
        if (li.a && li.childcontent) li.clickable = false;
    },

    positionSubmenu: function (li) {
        if (li.level0) {
            if (!window.isRTL) {
                //check position
                var lcor = li.getCoordinates();
                var ccor = li.childcontent.getCoordinates();
                if(!ccor.width)
                {
                    li.childcontent.setStyle ('display', 'block');
                    ccor = li.childcontent.getCoordinates();
                    li.childcontent.setStyle ('display', 'none');
                }

                var ml = 0;
                var l = lcor.left;
                var r = l + ccor.width;
                if (this.wrapper) {
                    var wcor = this.wrapper.getCoordinates();
                    l = l - wcor.left;
                    r = wcor.right - r + 10;
                } else {
                    r = window.getWidth() - r + 10;
                }
                if (l < 0 || l+r < 0) {
                    ml = - l;
                } else if (r < 0) {
                    ml = r;
                }
                if (ml != 0) li.childcontent.setStyle ('margin-left', ml);
            } else {
                //check position
                var lcor = li.getCoordinates();
                var ccor = li.childcontent.getCoordinates();
                if(!ccor.width)
                {
                    li.childcontent.setStyle ('display', 'block');
                    ccor = li.childcontent.getCoordinates();
                    li.childcontent.setStyle ('display', 'none');
                }
                var mr = 0;
                var r = lcor.right;
                var l = r - ccor.width;
                if (this.wrapper) {
                    var wcor = this.wrapper.getCoordinates();
                    l = l - wcor.left;
                    r = wcor.right - r + 10;
                } else {
                    r = window.getWidth() - r + 10;
                }
                if (r < 0 || l+r < 0) {
                    mr = - r;
                } else if (l < 0) {
                    mr = l;
                }
                if (mr != 0) li.childcontent.setStyle ('margin-right', mr);
            }
        } else {
            // Process submenu with level > 1
            if (window.isRTL) {
                // Window is RTL
                // If direction = left  & childcontent is out of viewport, change direction to right & view content to right
                // If direction = right & childcontent is out of viewport, change direction to left
                var direction = 'left';
                if (li.view_direction != undefined) {
                    direction = li.view_direction;
                }
                var lcor = li.getCoordinates();
                var ccor = li.childcontent.getCoordinates();
                if(!ccor.width)
                {
                    li.childcontent.setStyle ('display', 'block');
                    ccor = li.childcontent.getCoordinates();
                    li.childcontent.setStyle ('display', 'none');
                }
                if (direction == 'right') {
                    // Check out of viewport
                    var r = lcor.right + ccor.width;
                    if (this.wrapper) {
                        var wcor = this.wrapper.getCoordinates();
                        r = wcor.right - r + 10;
                    } else {
                        r = window.getWidth() - r + 10;
                    }
                    if (r < 0) {
                        // Change position for submenu
                        li.childcontent.setStyle ('margin-right', lcor.width);
                        // Change direction of submenu
                        var els = li.childcontent.getElements('li');
                        for (i = 0; i < els.length; i++) {
                            els[i].view_direction = 'left';
                        }
                    } else {
                        // Not out of viewport, however it is being right, so it need to be viewed in right
                        li.childcontent.setStyle('margin-right', -ccor.width + 20);
                    }
                } else {
                    // Check out of viewport in left direction
                    var l = lcor.left - ccor.width;
                    if (this.wrapper) {
                        var wcor = this.wrapper.getCoordinates();
                        l = l - wcor.left + 10;
                    } else {
                        l = l + 10;
                    }
                    if (l < 0) {
                        // Out of viewport, change position
                        li.childcontent.setStyle('margin-right', -ccor.width + 20);
                        // Change direction
                        var els = li.childcontent.getElements('li');
                        for (i = 0; i < els.length; i++) {
                            els[i].view_direction = 'right';
                        }
                    } else {
                        // Not viewport, however the position is not correct after initialize, so it need to change postion
                        li.childcontent.setStyle('margin-right', lcor.width);
                        // Level 3 is still have direction = right, so need to change submenu direction to left
                        var els = li.childcontent.getElements('li');
                        for (i = 0; i < els.length; i++) {
                            els[i].view_direction = 'left';
                        }
                    }
                }
            } else {
                // window isn't RTL
                // If direction == right and childcontent is out of view-port, change direction to left and view childcontent in left
                // If direction == left  and childcontent is out of view-port, change direction to right
                var direction = 'right';
                if (li.view_direction != undefined) {
                    direction = li.view_direction;
                }
                var lcor = li.getCoordinates();
                var ccor = li.childcontent.getCoordinates();
                if(!ccor.width)
                {
                    li.childcontent.setStyle ('display', 'block');
                    ccor = li.childcontent.getCoordinates();
                    li.childcontent.setStyle ('display', 'none');
                }
                if (direction == 'right') {
                    // Check out of viewport
                    var r = lcor.right + ccor.width;
                    if (this.wrapper) {
                        var wcor = this.wrapper.getCoordinates();
                        r = wcor.right - r + 10;
                    } else {
                        r = window.getWidth() - r + 10;
                    }
                    if (r < 0) {
                        // Change position for submenu
                        li.childcontent.setStyle ('margin-left', -ccor.width + 20);
                        // Change direction of submenu
                        var els = li.childcontent.getElements('li');
                        for (i = 0; i < els.length; i++) {
                            els[i].view_direction = 'left';
                        }
                    }
                } else {
                    // Check out of viewport in left direction
                    var l = lcor.left - ccor.width;
                    if (this.wrapper) {
                        var wcor = this.wrapper.getCoordinates();
                        l = l - wcor.left + 10;
                    } else {
                        l = l + 10;
                    }
                    if (l < 0) {
                        // Out of viewport, so change direction
                        var els = li.childcontent.getElements('li');
                        for (i = 0; i < els.length; i++) {
                            els[i].view_direction = 'right';
                        }
                    } else {
                        // Not out of viewport, however it is being left, so it need to be viewed in left
                        li.childcontent.setStyle('margin-left', -lcor.width - 20);
                    }
                }
            }
        }
    }
});








//Javacritpt Modulo mod_multitrans200


!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){function c(a){return h.raw?a:encodeURIComponent(a)}function d(a){return h.raw?a:decodeURIComponent(a)}function e(a){return c(h.json?JSON.stringify(a):String(a))}function f(a){0===a.indexOf('"')&&(a=a.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{a=decodeURIComponent(a.replace(b," "))}catch(c){return}try{return h.json?JSON.parse(a):a}catch(c){}}function g(b,c){var d=h.raw?b:f(b);return a.isFunction(c)?c(d):d}var b=/\+/g,h=a.cookie=function(b,f,i){if(void 0!==f&&!a.isFunction(f)){if(i=a.extend({},h.defaults,i),"number"==typeof i.expires){var j=i.expires,k=i.expires=new Date;k.setDate(k.getDate()+j)}return document.cookie=[c(b),"=",e(f),i.expires?"; expires="+i.expires.toUTCString():"",i.path?"; path="+i.path:"",i.domain?"; domain="+i.domain:"",i.secure?"; secure":""].join("")}for(var l=b?void 0:{},m=document.cookie?document.cookie.split("; "):[],n=0,o=m.length;o>n;n++){var p=m[n].split("="),q=d(p.shift()),r=p.join("=");if(b&&b===q){l=g(r,f);break}b||void 0===(r=g(r))||(l[q]=r)}return l};h.defaults={},a.removeCookie=function(b,c){return void 0!==a.cookie(b)?(a.cookie(b,"",a.extend({},c,{expires:-1})),!0):!1}});

function ie789(){for(var a=0,b=document.createElement("div");20>a;){y=b.innerHTML,l1=b.getElementsByTagName("p").length,b.innerHTML=y+"<!--[if gt IE "+a+"]><p></p><![endif]-->";var c=b.getElementsByTagName("p"),d=c.length;if(l1==d)break;++a}return l1}function back_btnx(a,b){if(2==b)if(ie789()<9&&ie789()>6)Cookie.dispose("googtrans"),Cookie.write("googtrans","cancel",{domain:a,duration:1}),location.reload();else{var d=document.getElementById(":2.container").contentDocument.getElementById(":2.restore");d.click()}else if(ie789()<9&&ie789()>6)jQuery.removeCookie("googtrans"),jQuery.cookie("googtrans","cancel",{domain:a,expires:1}),location.reload();else{var d=document.getElementById(":2.container").contentDocument.getElementById(":2.restore");d.click()}}function transman(a,b,c,d){if(2==a){var e=$$("select.goog-te-combo").get("value");e==c&&(ie789()<9&&ie789()>6?(Cookie.dispose("googtrans"),Cookie.write("googtrans","cancel",{domain:d,duration:1}),location.reload()):document.getElementById(":1.container").contentDocument.getElementById(":1.restore").click())}else{var e=jQuery(".goog-te-combo").val();e==c&&(ie789()<9&&ie789()>6?(jQuery.removeCookie("googtrans"),jQuery.cookie("googtrans","cancel",{domain:d,expires:1}),location.reload()):document.getElementById(":1.container").contentDocument.getElementById(":1.restore").click())}}

//Javacritpt Modulo mod_search
	 	
function buscarSalaDePrensa(str){
				
				
}
		
function oculatarBuscadorSala(){
				
}
function mostrarSala(){
			
}
jQuery(document).ready(function(){

	 		var timer;
	 		var valuet;
	 		jQuery("#mod-search-searchword2").bind('keyup', function() {

	 		 	
	 	        clearTimeout(timer);
	 	        var str = jQuery(this).val();

	 	        
	 	        if(str.length==0){
// 	 	        	$("#nfarticulosresult").html("");
	 	        	oculatarBuscadorSala();
	 	        }
	 	        	
	 	        if (str.length > 0 & valuet != str){
	 	            timer = setTimeout(function(){
	 	            	valuet = str;
	 	               
	 	            	mostrarSala();
	 	            	buscarSalaDePrensa(valuet);
	 	            	/*
	 	            	$.ajax({
	 	            		url:"/index.php",
	 	            		data : {
	 	            			"option" : "com_minambiente",
	 	            			"task" : "noticias.getLinkArticulosBuscar",
	 	            			"text" : valuet
	 	            		},

	 	            		
	 	            		
	 	            		success : function(result) {

	 	            			$("#nfarticulosresult").html(result);
	 	            			acccionLinknf();
	 	            			
	 	            		}
	 	            	});
	 	            		
	 	            	*/
	 	            	
	 	            	
	 	            	
	 	            }, 500);
	 	        }
	 	    });
	 	    
		 	});


function BuscarNoticias(){

}

//Javacritpt Modulo mod_menu htlm personalizado

function submenu2(id,proceso){
			document.getElementById('copcion1').style.display='none';
			document.getElementById('copcion2').style.display='none';
			document.getElementById('copcion3').style.display='none';
			document.getElementById('copcion4').style.display='none';
			document.getElementById('copcion5').style.display='none';
			document.getElementById('copcion6').style.display='none';
			document.getElementById('copcion7').style.display='none';
			document.getElementById('pruebafinal').style.display='none';
			document.getElementById('pruebafinal2').style.display='none';
			document.getElementById('normativa').style.display='none';
			document.getElementById('atencion').style.display='none';
			document.getElementById('opcion1').style.display='none';
			document.getElementById('areas').style.display='none';
			document.getElementById('otros').style.display='none';
			document.getElementById('opcionministerio').style.display='none';
			document.getElementById('pruebafinal').style.height='64px';
			document.getElementById('pruebafinal2').style.height='64px';
					if(id!='sala_prensa')
			{
				var ids=id.split(',');
				var tota_ids=ids.length;
				for(c=0;c<tota_ids;c++)
				{
				  capa=ids[c];
				  document.getElementById(capa).style.display=proceso;	
				}
			}
		}
		  function mostrar(id){
			  
			  if(document.getElementById(id)){
					document.getElementById(id).style.display='block';
			  }
		  }
		  function ocultar(id){
			 if(document.getElementById(id)){
			document.getElementById(id).style.display='none';
			 }
		  }
		  function mostrar2(id){
			document.getElementById('copcion1').style.display='none';
			document.getElementById('copcion2').style.display='none';
			document.getElementById('copcion3').style.display='none';
			document.getElementById('copcion4').style.display='none';
			document.getElementById('copcion5').style.display='none';
			document.getElementById('copcion6').style.display='none';
			document.getElementById('copcion7').style.display='none';
			 document.getElementById('copcion8').style.display='none';
			document.getElementById('tcopcion1').style.background='#ffffff';
			document.getElementById('tcopcion2').style.background='#ffffff';
			document.getElementById('tcopcion3').style.background='#ffffff';
			document.getElementById('tcopcion4').style.background='#ffffff';
			document.getElementById('tcopcion5').style.background='#ffffff';
			document.getElementById('tcopcion6').style.background='#ffffff';
			document.getElementById('tcopcion7').style.background='#ffffff';
			document.getElementById('tcopcion8').style.background='#ffffff';
			document.getElementById('acopcion1').style.color='#3b9900';
			document.getElementById('acopcion2').style.color='#3b9900';
			document.getElementById('acopcion3').style.color='#3b9900';
			document.getElementById('acopcion4').style.color='#3b9900';
			document.getElementById('acopcion5').style.color='#3b9900';
			document.getElementById('acopcion6').style.color='#3b9900';
			document.getElementById('acopcion7').style.color='#3b9900';
			document.getElementById('acopcion8').style.color='#3b9900';
			document.getElementById('a'+id).style.color='#666';
			document.getElementById(id).style.display='block';
			switch (id){
			  case "copcion1":
				document.getElementById('t'+id).style.background='#C4970F';
				document.getElementById('a'+id).style.color='#fff';
			  break;
			  case "copcion2":
				document.getElementById('t'+id).style.background='#00627d';
				document.getElementById('a'+id).style.color='#ffffff';
			  break;
			  case "copcion3":
				document.getElementById('t'+id).style.background='#557326';
				document.getElementById('a'+id).style.color='#ffffff';
			  break;
			  case "copcion4":
				document.getElementById('t'+id).style.background='#449184';
				document.getElementById('a'+id).style.color='#fff';
			  break;
			  case "copcion5":

				document.getElementById('t'+id).style.background='#00adf2';
				document.getElementById('a'+id).style.color='#ffffff';

			  break;
			  case "copcion6":
				document.getElementById('t'+id).style.background='#D13030';
				document.getElementById('a'+id).style.color='#ffffff';
			  break;
			  case "copcion7":
				document.getElementById('t'+id).style.background='#98B913';
				document.getElementById('a'+id).style.color='#fff';
			  break;
			}
		  }
		  function ocultar2(id){
			document.getElementById('opcion1').style.display='none';
			document.getElementById('opcionministerio').style.display='none';
			document.getElementById('copcion1').style.display='none';
			document.getElementById('copcion2').style.display='none';
			document.getElementById('copcion3').style.display='none';
			document.getElementById('copcion4').style.display='none';
			document.getElementById('copcion5').style.display='none';
			document.getElementById('copcion6').style.display='none';
			document.getElementById('copcion7').style.display='none';
			document.getElementById('copcion8').style.display='none';
			document.getElementById('areas').style.display='none';
			document.getElementById('otros').style.display='none';
			document.getElementById(id).style.display='none';
		  }
		  function vacio(id){
			document.getElementById('opcion1').style.display='none';
			document.getElementById('opcionministerio').style.display='none';
			document.getElementById('copcion1').style.display='none';
			document.getElementById('copcion2').style.display='none';
			document.getElementById('copcion3').style.display='none';
			document.getElementById('copcion4').style.display='none';
			document.getElementById('copcion5').style.display='none';
			document.getElementById('copcion6').style.display='none';
			document.getElementById('copcion7').style.display='none';
			 document.getElementById('copcion8').style.display='none';
			 document.getElementById('areas').style.display='none';
			 document.getElementById('otros').style.display='none';
			document.getElementById(id).background="/minambiente_site/images/bgover-menu.jpg";
		  }

<?php
$this->setGenerator(null);
$iduserax= JFactory::getUser()->id;
if(empty($iduserax)){
	$app = JFactory::getApplication();
	$app->redirect('index.php', false);
	die();
}
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
	$span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span9";
}
else
{
	$span = "span12";
}


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-11686645-4', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
var rv = -1;  //inicializamos para prevenir fallos de puntero a nulo
if (navigator.appName == 'Microsoft Internet Explorer')
{
     var ua = navigator.userAgent;
     var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
     if (re.exec(ua) != null)
              rv = parseFloat( RegExp.$1 );
     if(rv > -1){
         switch(rv){
             case 7.0:
                 
               	alert("La plataforma de la escuela de formaci\u00f3n se encuentra disponible para navegadores desde la versi\u00f3n 8, a continuaci\u00f3n redireccionaremos a una p\u00e1gina que le permitir\u00e1 actualizar su navegador, para visualizar los m\u00f3dulos del curso de cambio clim\u00e1tico");           	
               	window.location = "http://windows.microsoft.com/es-es/internet-explorer/download-ie";
                break;
             case 6.0: 
            	 alert("La plataforma de la escuela de formaci\u00f3n se encuentra disponible para navegadores desde la versi\u00f3n 8, a continuaci\u00f3n redireccionaremos a una p\u00e1gina que le permitir\u00e1 actualizar su navegador, para visualizar los m\u00f3dulos del curso de cambio clim\u00e1tico");
                 window.location = "http://windows.microsoft.com/es-es/internet-explorer/download-ie";
                 break;
             default:
                 
                  break;                  
         }
     }
}
</script>
	
<title>Escuela de Formaci&oacute;n</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon-minis.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<?php
	// Use of Google Font
	if ($this->params->get('googleFont'))
	{
	?>
		<link href='http://fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName');?>' rel='stylesheet' type='text/css' />
		<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName'));?>', sans-serif;
			}
		</style>
	<?php
	}
	?>
	<?php
	// Template color
	if ($this->params->get('templateColor'))
	{
	?>
	<style type="text/css">
		a
		{
			color: <?php echo $this->params->get('templateColor');?>;
		}
		.navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
		.btn-primary
		{
			background: <?php echo $this->params->get('templateColor');?>;
		}
		.navbar-inner
		{
			-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
		}
	</style>
	<?php
	}
	?>
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
     
  </head>

<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '');
?>">

	<!-- Body -->
    <div class="body">
		<div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : '');?>">
			<!-- Header -->
			<div class="header">
				<div class="header-inner clearfix">
					<div id="content_cab">
                    
                    <div id="content-izq">
                    <div style="background-color: #fff;border-radius: 20px; margin-left: 19px; margin-bottom: 15px; width: 85%;">
                      <img style="margin-left:-46px; margin-bottom:10px;" src="/templates/minambiente-aula-moodle/images/escudo-ministerio.png">
                    </div>
                    <div style="top: 8px; position: relative;color:#fff; margin: -3px 0 15px 20px; font-size: 30px; font-weight: 100; letter-spacing: 0.2em;">Escuela de Formación Virtual</div>
                    	<jdoc:include type="modules" name="aula" style="none" />
                     </div>   
                     <div id="content-der">
                     	<div id="registro"><jdoc:include type="modules" name="registro" style="none" /></div>
                        <div id="content_img_login">.</div>
                        <div id="foto"><jdoc:include type="modules" name="foto" style="none" /></div>
                        
                     </div>
                     </div>
                    </div>
                    </div>
                    
                    
                    <jdoc:include type="message" />
                    	<div id="fondo-contenido">  
                        	<div id="izq">
                   					<jdoc:include type="modules" name="iconos" style="none" />
							</div>     
                                                          
                			<div id="contenido">
                    			<div id="content-main">
									<jdoc:include type="component" />
                       			</div>
                   			 </div>
                        </div>
                    
                    <div id="banner-logos">
						<jdoc:include type="modules" name="banner-logos" style="none" />
					</div>
                    <div id="politicas">
						<jdoc:include type="modules" name="politicas" style="none" />
					</div>
                    <div id="pie">
				<jdoc:include type="modules" name="pie" style="none" />
            		</div>
				</div>
			</div>
				
	<!-- Footer -->		
		
</body>
</html>
